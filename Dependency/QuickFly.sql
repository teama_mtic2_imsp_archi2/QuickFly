-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 02 Avril 2019 à 17:17
-- Version du serveur :  10.1.38-MariaDB-0ubuntu0.18.04.1
-- Version de PHP :  7.2.15-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `QuickFly`
--
CREATE DATABASE IF NOT EXISTS `QuickFly` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `QuickFly`;

-- --------------------------------------------------------

--
-- Structure de la table `Bagage`
--

CREATE TABLE `Bagage` (
  `id` int(5) NOT NULL,
  `typeB` varchar(30) NOT NULL,
  `poidsB` double NOT NULL,
  `idUser` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `avion`
--

CREATE TABLE `avion` (
  `id` int(4) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `compagnie` varchar(255) DEFAULT NULL,
  `categorie` varchar(30) DEFAULT NULL,
  `typeAvion` varchar(30) DEFAULT NULL,
  `place` int(3) DEFAULT NULL,
  `poidsMax` double DEFAULT NULL,
  `surplus` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `avion`
--

INSERT INTO `avion` (`id`, `nom`, `compagnie`, `categorie`, `typeAvion`, `place`, `poidsMax`, `surplus`) VALUES
(1, 'kq', 'kenyairways', 'AirLourd', 'AirPressurise', 500, 70, 1),
(2, 'rwdair', 'rwandair_express', 'AirLeger', 'AirCargo', 200, 23.5, 0),
(3, 'ethair', 'ethiopian_airlines', 'AirMoyen', 'AirRoulant', 300, 42, 0),
(4, 'zenair', 'zenirair', 'AirLourd', 'AirVivant', 320, 70, 1),
(5, 'coair', 'trans_air_congo', 'AirLourd', 'AirVivant', 450, 65, 0),
(6, 'sgair', 'senegal_airlines', 'AirMoyen', 'AirCargo', 280, 42, 0),
(7, 'ngair', 'niger_ailines', 'AirLeger', 'AirRoulant', 130, 23.5, 0),
(8, 'ngriair', 'nigeria_airlines', 'AirMoyen', 'AirCargo', 250, 42, 0),
(9, 'algair', 'algerie_airlines', 'AirLourd', 'AirRoulant', 480, 55, 0),
(10, 'shair', 'sahara_airlines', 'AirMoyen', 'AirVivant', 330, 42, 0),
(11, 'exair', 'air_excel', 'AirMoyen', 'AirCargo', 270, 42, 0),
(12, 'tzair', 'air_tanzania', 'AirLourd', 'AirVivant', 440, 60, 1),
(13, 'salair', 'as_salaam_air', 'AirLeger', 'AirCargo', 165, 23.5, 0),
(14, 'arcair', 'auric_air', 'AirMoyen', 'AirRoulant', 290, 42, 0),
(15, 'ctlair', 'costal_aviation', 'AirLeger', 'AirPressurise', 165, 23.5, 0),
(16, 'fsair', 'fastjet_tanzania', 'AirLourd', 'AirVivant', 490, 65, 1),
(17, 'prcsair', 'precision_air', 'AirMoyen', 'AirVivant', 355, 42, 0),
(18, 'kens', 'kenyairways', 'AirLourd', 'AirRoulant', 5000, 56, 42),
(19, 'titus', 'coastal_aviation', 'AirLeger', 'AirCargo', 200, 18, 1),
(20, 'sumas', 'air_tanzania', 'AirLourd', 'AirVivant', 600, 70, 0),
(21, 'curstos', 'sahara_airlines', 'AirMoyen', 'AirPressurise', 250, 35, 1),
(22, 'naja', 'nigeria_airlines', 'AirMoyen', 'AirRoulant', 195, 25, 0),
(23, 'narech', 'algerie_airlines', 'AirLourd', 'AirCargo', 480, 67, 0),
(24, 'powerful', 'air_excel', 'AirLeger', 'AirVivant', 140, 45, 1),
(25, 'guide', 'safari_plus', 'AirLeger', 'AirRoulant', 325, 13, 0),
(26, 'tor', 'zenith_air', 'AirLourd', 'Airvivant', 320, 72, 1),
(27, 'bonam', 'gouanzou_airlines', 'AirMoyen', 'AirCargo', 450, 60, 0),
(28, 'wolkork', 'senegal_airlines', 'AirLeger', 'AirRoullant', 135, 20, 1);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(8) NOT NULL,
  `idUser` int(6) NOT NULL,
  `idVol` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `reservation`
--

INSERT INTO `reservation` (`id`, `idUser`, `idVol`) VALUES
(1, 15, 1),
(2, 6, 14),
(3, 10, 29),
(4, 14, 12),
(5, 13, 5),
(6, 9, 11),
(7, 1, 18),
(8, 4, 2),
(9, 21, 7),
(10, 11, 19),
(11, 17, 4),
(12, 20, 6),
(13, 12, 9),
(14, 16, 13),
(15, 7, 25),
(16, 5, 28),
(17, 2, 26),
(18, 14, 4),
(19, 25, 2),
(20, 23, 8),
(21, 1, 5),
(22, 5, 1),
(23, 4, 17),
(24, 9, 4),
(25, 17, 21);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(6) NOT NULL,
  `typeUser` varchar(10) NOT NULL DEFAULT 'client',
  `mail` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `typeUser`, `mail`, `mdp`) VALUES
(1, 'client', 'emma@gmail.com', 'imsp123'),
(2, 'client', 'christian@gmail.com', 'imsp12'),
(3, 'client', 'yao@gmail.com', 'imsp0'),
(4, 'admin', 'sidoine.ode@imsp-uac.org', '66091913'),
(5, 'client', 'imsp1@gmail.com', 'imsp1'),
(6, 'client', 'imsp2@gmail.com', 'imsp2'),
(7, 'client', 'imsp3@gmail.com', 'imsp3'),
(8, 'client', 'imsp4gmail.com', 'imsp4'),
(9, 'client', 'imsp5@gmail.com', 'imsp5'),
(10, 'client', 'imsp6@gmail.com', 'imsp6'),
(11, 'client', 'imsp7@gmail.com', 'imsp7'),
(12, 'client', 'imsp8@gmail.com', 'imsp8'),
(13, 'client', 'imsp9@gmail.com', 'imsp9'),
(14, 'client', 'imsp10@gmail.com', 'imsp10'),
(15, 'client', 'imsp11@gmail.com', 'imsp11'),
(16, 'client', 'imsp12@gmail.com', 'imsp12'),
(17, 'client', 'imsp13@gmail.com', 'imsp13'),
(18, 'client', 'imsp14@gmail.com', 'imsp14'),
(19, 'client', 'imsp15@gmail.com', 'imsp15'),
(20, 'client', 'imsp16@gmail.com', 'imsp16'),
(21, 'client', 'imsp17@gmail.com', 'imsp17'),
(22, 'client', 'imsp18@gmail.com', 'imsp18'),
(23, 'client', 'imsp19@gmail.com', 'imsp19'),
(24, 'client', 'imsp20@gmail.com', 'imsp20'),
(25, 'client', 'imsp21@gmail.com', 'imsp21'),
(26, 'admin', 'administ@gmail.com', 'imsproi'),
(27, 'admin', 'roch@gmail.com', 'suede1573'),
(28, 'admin', 'daniel@gmail.com', 'nico1290'),
(29, 'admin', 'anastasia@gmail.com', 'riojat'),
(30, 'admin', 'juvenal@gmail.com', 'cococarib'),
(31, 'client', 'osidoine@gmail.com', '66091913'),
(32, 'client', 'caroline@imsp-uac.org', '66091913'),
(33, 'client', 'beryl@gmail.com', 'berezina'),
(34, 'client', 'honore@gmail.com', '123456'),
(35, 'client', 'odes@gmail.com', '66091913');

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

CREATE TABLE `vol` (
  `id` int(6) NOT NULL,
  `villeD` varchar(255) NOT NULL,
  `villeA` varchar(255) NOT NULL,
  `dateD` date NOT NULL,
  `dateA` date NOT NULL,
  `aeroportD` varchar(30) NOT NULL,
  `aeroportA` varchar(30) NOT NULL,
  `prix` double NOT NULL,
  `tempsEscale` int(5) DEFAULT NULL,
  `idAvion` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `vol`
--

INSERT INTO `vol` (`id`, `villeD`, `villeA`, `dateD`, `dateA`, `aeroportD`, `aeroportA`, `prix`, `tempsEscale`, `idAvion`) VALUES
(1, 'bujumbura', 'nairobi', '2019-04-01', '2019-04-01', 'bujumbura', 'nairobi', 320000, 3, 1),
(2, 'nairobi', 'cotonou', '2019-04-01', '2019-04-02', 'nairobi', 'cotonou', 500000, 0, 1),
(3, 'bujumbura', 'cotonou', '2019-04-03', '2019-04-04', 'bujumbura', 'cotonou', 820000, 0, 4),
(4, 'cotonou', 'bujumbura', '2019-04-04', '2019-04-05', 'cotonou', 'bujumbura', 150000, 0, 4),
(5, 'cotonou', 'nairobi', '2019-04-05', '2019-04-05', 'cotonou', 'nairobi', 500000, 0, 8),
(6, 'nairobi', 'bujumbura', '2019-04-05', '2019-04-06', 'nairobi', 'bujumbura', 300000, 0, 8),
(7, 'abuja', 'nairobi', '2019-04-07', '2019-04-07', 'abuja', 'nairobi', 180000, 4, 8),
(8, 'nairobi', 'bujumbura', '2019-04-07', '2019-04-08', 'nairobi', 'bujumbura', 300000, 0, 2),
(9, 'casablanca', 'nairobi', '2019-04-09', '2019-04-10', 'casablanca', 'nairobi', 250000, 5, 10),
(10, 'nairobi', 'bujumbura', '2019-04-10', '2019-04-10', 'nairobi', 'bujumbura', 250000, 0, 11),
(11, 'lagos', 'cotonou', '2019-04-11', '2019-04-11', 'lagos', 'cotonou', 280000, 0, 18),
(12, 'lagos', 'nairobi', '2019-04-12', '2019-04-13', 'lagos', 'nairobi', 350000, 5, 4),
(13, 'nairobi', 'bujumbura', '2019-04-13', '2019-04-13', 'nairobi', 'bujumbura', 200000, 0, 12),
(14, 'caire', 'cotonou', '2019-04-14', '2019-04-14', 'caire', 'cotonou', 300000, 0, 14),
(15, 'addis abeba', 'nairobi', '2019-04-15', '2019-04-15', 'addis abeba', 'nairobi', 350000, 2, 9),
(16, 'nairobi', 'dar-es-salam', '2019-04-15', '2019-04-16', 'nairobi', 'dar-es-salam', 700000, 0, 13),
(17, 'kigali', 'cotonou', '2019-04-17', '2019-04-17', 'kigali', 'cotonou', 500000, 0, 2),
(18, 'kigali', 'bujumbura', '2019-04-18', '2019-04-18', 'kigali', 'bujumbura', 300000, 0, 5),
(19, 'brazaville', 'bamako', '2019-04-20', '2019-04-20', 'brazaville', 'bamako', 150000, 0, 20),
(20, 'marakech', 'dakar', '2019-04-21', '2019-04-21', 'marakech', 'dakar', 350000, 3, 25),
(21, 'addis abeba', 'gounanzou', '2019-04-22', '2019-04-23', 'addis abeba', 'gouanzou', 700000, 7, 2),
(22, 'addis abeba', 'gounanzou', '2019-04-22', '2019-04-23', 'addis abeba', 'gouanzou', 700000, 7, 2),
(23, 'libreville', 'kigali', '2019-04-23', '2019-04-23', 'libreville', 'kigali', 320000, 0, 5),
(24, 'abuja', 'abidjan', '2019-04-24', '2019-04-24', 'abuja', 'abijan', 200000, 0, 8),
(25, 'dakar', 'cotonou', '2019-04-21', '2019-04-22', 'dakar', 'cotonou', 250000, 0, 3),
(26, 'cotonou', 'addis abeba', '2019-04-21', '2019-04-22', 'cotonou', 'addis abeba', 400000, 4, 23),
(27, 'addis abeba', 'gounanzou', '2019-04-22', '2019-04-23', 'addis abeba', 'gouanzou', 700000, 7, 20),
(28, 'gounanzou', 'addis abeba', '2019-04-24', '2019-04-25', 'gounazou', 'addis abeba', 500000, 7, 18),
(29, 'dakar', 'abidjan', '2019-04-21', '2019-04-21', 'dakar', 'abidjan', 230000, 3, 11),
(30, 'abidjan', 'cotonou', '2019-04-21', '2019-04-22', 'abibjan', 'cotonou', 200000, 0, 21),
(31, 'cotonou', 'dakar', '2019-04-23', '2019-04-24', 'cotonou', 'dakar', 230000, 0, 6),
(32, 'kigali', 'libreville', '2019-04-23', '2019-04-24', 'kigali', 'libreville', 300000, 0, 19),
(33, 'ouagadougou', 'niamey', '2019-04-25', '2019-04-25', 'ouagadougou', 'niamey', 120000, 2, 7),
(34, 'niamey', 'cotonou', '2019-04-25', '2019-04-25', 'niamey', 'cotonou', 85000, 0, 7),
(35, 'cotonou', 'accra', '2019-04-26', '2019-04-26', 'cotonou', 'accra', 100000, 4, 10),
(36, 'accra', 'libreville', '2019-04-26', '2019-04-27', 'accra', 'libreville', 240000, 6, 5),
(37, 'libreville', 'njamena', '2019-04-27', '2019-04-28', 'libreville', 'njamena', 350000, 3, 14),
(38, 'njamena', 'libreville', '2019-04-27', '2019-04-28', 'njamena', 'libreville', 350000, 3, 14),
(39, 'libreville', 'accra', '2019-04-29', '2019-04-30', 'libreville', 'accra', 240000, 6, 17),
(40, 'accra', 'cotonou', '2019-04-30', '2019-05-01', 'accra', 'cotonou', 100000, 0, 8);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Bagage`
--
ALTER TABLE `Bagage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idUser` (`idUser`);

--
-- Index pour la table `avion`
--
ALTER TABLE `avion`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idVol` (`idVol`),
  ADD KEY `idUser` (`idUser`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vol`
--
ALTER TABLE `vol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idAvion` (`idAvion`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Bagage`
--
ALTER TABLE `Bagage`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `avion`
--
ALTER TABLE `avion`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pour la table `vol`
--
ALTER TABLE `vol`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Bagage`
--
ALTER TABLE `Bagage`
  ADD CONSTRAINT `Bagage_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`idVol`) REFERENCES `vol` (`id`),
  ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `vol`
--
ALTER TABLE `vol`
  ADD CONSTRAINT `vol_ibfk_1` FOREIGN KEY (`idAvion`) REFERENCES `avion` (`id`);
--
-- Base de données :  `db_billetavion`
--
CREATE DATABASE IF NOT EXISTS `db_billetavion` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `db_billetavion`;

-- --------------------------------------------------------

--
-- Structure de la table `avion`
--

CREATE TABLE `avion` (
  `type_avion` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL,
  `poids_bagage` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `nombredeplace` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

CREATE TABLE `compagnie` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `compagnie`
--

INSERT INTO `compagnie` (`id`, `email`, `nom`, `tel`) VALUES
(2, 'airmaroc@gmail.com', 'AirMaroc', '+12464797590'),
(3, 'airbruxele@gmail.com', 'AirBruxele', '+25564797590'),
(4, 'airusa@usa.gov', 'AirUSA', '+1 325665888');

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(9),
(9),
(9),
(9),
(9);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id` bigint(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `etat` varchar(255) DEFAULT NULL,
  `nombredeplace` int(11) DEFAULT NULL,
  `id_utilisateur` bigint(20) DEFAULT NULL,
  `id_vol` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `type_utilisateur` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `motdepasse` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`type_utilisateur`, `id`, `email`, `motdepasse`) VALUES
('client', 5, 'osidoine@gmail.com', 'osidoine'),
('client', 6, 'mohazam@gmail.com', 'mohazam'),
('admin', 7, 'sidoine@gmail.com', 'sidoine'),
('agent', 8, 'Laoualy@gmail.com', 'Laoualy');

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

CREATE TABLE `vol` (
  `id` bigint(20) NOT NULL,
  `datea` datetime DEFAULT NULL,
  `dated` datetime DEFAULT NULL,
  `duree` varchar(255) DEFAULT NULL,
  `nombredeplace` int(11) DEFAULT NULL,
  `poids_bagage` varchar(255) DEFAULT NULL,
  `prix` double NOT NULL,
  `villea` varchar(255) DEFAULT NULL,
  `villed` varchar(255) DEFAULT NULL,
  `id_avion` bigint(20) DEFAULT NULL,
  `id_compagnie` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `avion`
--
ALTER TABLE `avion`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `compagnie`
--
ALTER TABLE `compagnie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK3kt9mawec4sqdm8j6830e8yp9` (`id_utilisateur`),
  ADD KEY `FK6w89wb2jeg31kddarxq85d66o` (`id_vol`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vol`
--
ALTER TABLE `vol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK8f1ehb8k2sv98y3ju7agkc3rm` (`id_avion`),
  ADD KEY `FK4jtfrxwojjgt31mbju8p1x9md` (`id_compagnie`);
--
-- Base de données :  `db_locationvoiture_mvc`
--
CREATE DATABASE IF NOT EXISTS `db_locationvoiture_mvc` DEFAULT CHARACTER SET utf16 COLLATE utf16_general_ci;
USE `db_locationvoiture_mvc`;

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

--
-- Contenu de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(17),
(17),
(17);

-- --------------------------------------------------------

--
-- Structure de la table `location`
--

CREATE TABLE `location` (
  `id` bigint(20) NOT NULL,
  `date_location` datetime DEFAULT NULL,
  `date_retour` datetime DEFAULT NULL,
  `personne_id` bigint(20) NOT NULL,
  `voiture_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `id` bigint(20) NOT NULL,
  `age` bigint(20) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `telephone` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Structure de la table `voiture`
--

CREATE TABLE `voiture` (
  `id` bigint(20) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `marque` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `prix` bigint(20) NOT NULL,
  `puissance` bigint(20) NOT NULL,
  `vitesse` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

--
-- Contenu de la table `voiture`
--

INSERT INTO `voiture` (`id`, `categorie`, `marque`, `photo`, `prix`, `puissance`, `vitesse`) VALUES
(1, 'Voiture de sortie', 'MDW', 'Capture du 2018-08-02 15-02-54.png', 100000, 600, 150),
(2, 'Voiture de sortie', 'MDW', 'Capture du 2018-08-02 15-02-54.png', 100000, 600, 150),
(3, 'voiture de luxe', 'Lamboguini', 'Capture du 2018-06-28 13-51-15.png', 500000, 500, 115),
(4, 'voiture de luxe', 'Peugeot', 'Capture du 2018-06-28 13-51-15.png', 500000, 500, 155),
(5, 'voiture de luxe', 'Lamboguini', 'Capture du 2018-06-28 13-50-42.png', 587574554, 822, 500),
(6, 'voiture de sport', 'Peugeot', 'Capture du 2018-06-28 13-50-42.png', 5000000, 600, 458),
(7, 'Voiture de sortie', 'Peugeot', 'Capture du 2018-06-28 13-51-15.png', 1547854, 654, 478),
(15, 'voiture de luxe', 'Peugeot', 'Capture du 2018-06-28 13-50-42.png', 5000000, 600, 500),
(16, 'Voiture de sortie', 'Lamboguini', 'Capture du 2018-08-02 14-59-15.png', 5874517, 7000, 457);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKwvsncudxd9kavyt55xlwm5m1` (`personne_id`),
  ADD KEY `FKei6x977yqtpvkdkncbi5cq3kr` (`voiture_id`);

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `voiture`
--
ALTER TABLE `voiture`
  ADD PRIMARY KEY (`id`);
--
-- Base de données :  `db_safeschool_master`
--
CREATE DATABASE IF NOT EXISTS `db_safeschool_master` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `db_safeschool_master`;

-- --------------------------------------------------------

--
-- Structure de la table `apprenant`
--

CREATE TABLE `apprenant` (
  `id` int(11) NOT NULL,
  `tuteur_id` int(11) DEFAULT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sexe` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_rue` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_ville` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `date_naissance` date NOT NULL,
  `lieu_naissance` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nationalite` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `apprenant_exploit`
--

CREATE TABLE `apprenant_exploit` (
  `apprenant_id` int(11) NOT NULL,
  `exploit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE `avis` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_publication` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

CREATE TABLE `classe` (
  `id` int(11) NOT NULL,
  `code_classe` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `libelle_classe` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `commentaire_exploit`
--

CREATE TABLE `commentaire_exploit` (
  `id` int(11) NOT NULL,
  `exploit_apprenant_id` int(11) DEFAULT NULL,
  `contenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cursus_apprenant`
--

CREATE TABLE `cursus_apprenant` (
  `id` int(11) NOT NULL,
  `etablissement_apprenant_id` int(11) DEFAULT NULL,
  `sous_classe_id` int(11) DEFAULT NULL,
  `ets_classe_niveau_specialite_id` int(11) DEFAULT NULL,
  `decision` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annee_scolaire` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statut` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `moyenne_generale` decimal(10,2) NOT NULL,
  `classe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `dirigeant`
--

CREATE TABLE `dirigeant` (
  `id` int(11) NOT NULL,
  `fonction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_publication` date NOT NULL,
  `taille` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `typeDocument` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domaine` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matiere` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_epreuve` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_expose` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `domaine`
--

CREATE TABLE `domaine` (
  `id` int(11) NOT NULL,
  `code_domaine` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `libelle_domaine` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `domaine_sous_domaine`
--

CREATE TABLE `domaine_sous_domaine` (
  `domaine_id` int(11) NOT NULL,
  `sous_domaine_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `enseigner`
--

CREATE TABLE `enseigner` (
  `id` int(11) NOT NULL,
  `sous_classe_id` int(11) DEFAULT NULL,
  `etablissement_prof_id` int(11) DEFAULT NULL,
  `date_enseigner` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement`
--

CREATE TABLE `etablissement` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_rue` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_ville` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_postale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `telephone1` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `telephone2` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_site` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `annee_creation` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `historique` longtext COLLATE utf8_unicode_ci NOT NULL,
  `statut_etablissement` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categorie_etablissement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `niveau_etablissement` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo_etablissement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_couverture_etablissement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pourcentage_devoir` decimal(10,2) DEFAULT NULL,
  `pourcentage_partiel` decimal(10,2) DEFAULT NULL,
  `pourcentage_tp` decimal(10,2) DEFAULT NULL,
  `longitude` decimal(10,2) DEFAULT NULL,
  `latitude` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement_apprenant`
--

CREATE TABLE `etablissement_apprenant` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `apprenant_id` int(11) DEFAULT NULL,
  `frais_scolarite` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement_classe`
--

CREATE TABLE `etablissement_classe` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `classe_id` int(11) DEFAULT NULL,
  `frais_scolarite` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement_classe_niveau`
--

CREATE TABLE `etablissement_classe_niveau` (
  `id` int(11) NOT NULL,
  `etablissement_classe_id` int(11) DEFAULT NULL,
  `niveau_etude_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `libelle` varchar(6) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement_classe_niveau_specialite`
--

CREATE TABLE `etablissement_classe_niveau_specialite` (
  `id` int(11) NOT NULL,
  `ets_classe_niveau_id` int(11) DEFAULT NULL,
  `specialite_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement_dirigeant`
--

CREATE TABLE `etablissement_dirigeant` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `dirigeant_id` int(11) DEFAULT NULL,
  `annee` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement_prof`
--

CREATE TABLE `etablissement_prof` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `prof_id` int(11) DEFAULT NULL,
  `matiere_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `examen`
--

CREATE TABLE `examen` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `annee_scolaire` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_presente` int(11) NOT NULL,
  `nombre_admis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `exploit`
--

CREATE TABLE `exploit` (
  `id` int(11) NOT NULL,
  `sous_domaine_id` int(11) DEFAULT NULL,
  `titre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_publication` datetime NOT NULL,
  `point` int(11) NOT NULL,
  `niveau` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `type_auteur` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `exploit_apprenant`
--

CREATE TABLE `exploit_apprenant` (
  `id` int(11) NOT NULL,
  `exploit_id` int(11) DEFAULT NULL,
  `apprenant_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fichier`
--

CREATE TABLE `fichier` (
  `id` int(11) NOT NULL,
  `exploit_id` int(11) DEFAULT NULL,
  `information_id` int(11) DEFAULT NULL,
  `apprenant_id` int(11) DEFAULT NULL,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `chemin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_stockage` date NOT NULL,
  `type_fichier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_photo_couverture` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fondateur_ou_directeur`
--

CREATE TABLE `fondateur_ou_directeur` (
  `id` int(11) NOT NULL,
  `fonction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `information`
--

CREATE TABLE `information` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_publication` date NOT NULL,
  `type_information` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `publie` tinyint(1) NOT NULL,
  `destinataire` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annee_scolaire` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `id` int(11) NOT NULL,
  `ets_classe_niveau_specialite_id` int(11) DEFAULT NULL,
  `code_matiere` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `libelle_matiere` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `credit` int(11) NOT NULL,
  `masse_horaire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `moyenne_periodique_apprenant`
--

CREATE TABLE `moyenne_periodique_apprenant` (
  `id` int(11) NOT NULL,
  `apprenant_cursus_id` int(11) DEFAULT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `moyenne` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `niveau_etude`
--

CREATE TABLE `niveau_etude` (
  `id` int(11) NOT NULL,
  `code_niveau_etude` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `libelle_niveau_etude` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

CREATE TABLE `note` (
  `id` int(11) NOT NULL,
  `apprenant_cursus_id` int(11) DEFAULT NULL,
  `matiere_id` int(11) DEFAULT NULL,
  `note` double NOT NULL,
  `type_note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_note` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime DEFAULT NULL,
  `libelle` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `annee_scolaire` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boucle` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `prof`
--

CREATE TABLE `prof` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sexe` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_rue` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_ville` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `date_naissance` date NOT NULL,
  `lieu_naissance` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `programme_cours`
--

CREATE TABLE `programme_cours` (
  `id` int(11) NOT NULL,
  `enseigner_id` int(11) DEFAULT NULL,
  `salle_id` int(11) DEFAULT NULL,
  `date_programme` datetime NOT NULL,
  `heure_debut` time NOT NULL,
  `heure_fin` time NOT NULL,
  `jour` varchar(8) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `code_salle` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nbre_places` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sous_classe`
--

CREATE TABLE `sous_classe` (
  `id` int(11) NOT NULL,
  `etablissement_classe_niveau_id` int(11) DEFAULT NULL,
  `code_sous_classe` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `libelle_sous_classe` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sous_domaine`
--

CREATE TABLE `sous_domaine` (
  `id` int(11) NOT NULL,
  `code_sousdomaine` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `libelle_sousdomaine` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

CREATE TABLE `specialite` (
  `id` int(11) NOT NULL,
  `code_specialite` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `libelle_specialite` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `stat_periodique`
--

CREATE TABLE `stat_periodique` (
  `id` int(11) NOT NULL,
  `sous_classe_id` int(11) DEFAULT NULL,
  `periode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `effectif_total` int(11) NOT NULL,
  `moyenne` decimal(4,2) NOT NULL,
  `nombre_apprenant_ayant_moyenne` int(11) NOT NULL,
  `annee_scolaire` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tuteur`
--

CREATE TABLE `tuteur` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sexe` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_rue` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_ville` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `date_naissance` date NOT NULL,
  `lieu_naissance` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `profession` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lien_parente` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `uploaded_file`
--

CREATE TABLE `uploaded_file` (
  `id` int(11) NOT NULL,
  `parent_folder_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `srcpath_or_oldname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `uploaded_file`
--

INSERT INTO `uploaded_file` (`id`, `parent_folder_id`, `name`, `path`, `srcpath_or_oldname`, `action`, `created_at`, `last_modified`, `type`) VALUES
(1, NULL, 'Drive', '/', NULL, 'create', '2018-11-05 00:00:00', '2018-11-05 00:00:00', 'folder'),
(2, NULL, 'sidoine', '/', NULL, 'user', '2018-11-05 17:13:20', '2018-11-05 17:13:20', 'folder'),
(3, 2, 'MEDIATHEQUE', 'sidoine', NULL, 'create', '2018-11-05 17:13:20', '2018-11-05 17:13:20', 'folder'),
(4, 3, 'Corbeille', 'sidoine/MEDIATHEQUE', NULL, 'create', '2018-11-05 17:13:20', '2018-11-05 17:13:20', 'folder'),
(5, 3, 'Documents', 'sidoine/MEDIATHEQUE', NULL, 'create', '2018-11-05 17:13:54', '2018-11-05 17:13:54', 'folder'),
(6, 3, 'Musique', 'sidoine/MEDIATHEQUE', NULL, 'create', '2018-11-05 17:14:29', '2018-11-05 17:14:29', 'folder'),
(7, 3, 'Vidéos', 'sidoine/MEDIATHEQUE', NULL, 'create', '2018-11-05 17:15:19', '2018-11-05 17:15:19', 'folder'),
(8, 3, 'Photos', 'sidoine/MEDIATHEQUE', NULL, 'create', '2018-11-05 17:16:36', '2018-11-05 17:16:36', 'folder'),
(9, 3, 'Autres', 'sidoine/MEDIATHEQUE', NULL, 'create', '2018-11-05 17:17:25', '2018-11-05 17:17:25', 'folder'),
(10, 3, 'Private', 'sidoine/MEDIATHEQUE', NULL, 'create', '2018-11-05 17:17:40', '2018-11-05 17:17:40', 'folder'),
(11, 8, '00logo1.png', 'sidoine/MEDIATHEQUE/Photos', NULL, 'upload', '2018-11-05 17:18:23', '2018-11-05 17:18:23', 'file'),
(12, 8, 'Capture du 2018-08-06 23-04-35.png', 'sidoine/MEDIATHEQUE/Photos', NULL, 'upload', '2018-11-05 17:20:28', '2018-11-05 17:20:28', 'file'),
(13, 8, '5gw2jskc.jpg', 'sidoine/MEDIATHEQUE/Photos', NULL, 'upload', '2018-11-05 17:20:29', '2018-11-05 17:20:29', 'file'),
(14, 8, '1.jpg', 'sidoine/MEDIATHEQUE/Photos', NULL, 'upload', '2018-11-05 17:20:29', '2018-11-05 17:20:29', 'file'),
(15, 8, '2011-Affiche-Bal-St-Valentin.jpg', 'sidoine/MEDIATHEQUE/Photos', NULL, 'upload', '2018-11-05 17:20:29', '2018-11-05 17:20:29', 'file'),
(16, 8, '5dfc583c.gif', 'sidoine/MEDIATHEQUE/Photos', NULL, 'upload', '2018-11-05 17:20:29', '2018-11-05 17:20:29', 'file'),
(17, 6, 'LORIE - I love You. - Copie.mp3', 'sidoine/MEDIATHEQUE/Musique', NULL, 'upload', '2018-11-05 17:24:14', '2018-11-05 17:24:14', 'file');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `apprenant_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `user_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `apprenant_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `user_type`) VALUES
(1, NULL, 'sidoine', 'sidoine', 'osidoine@gmail.com', 'osidoine@gmail.com', 1, '98vm94hf93wggskoss0kgwkw80o8so', '$2y$13$98vm94hf93wggskoss0kguJ5ZFpMmpOYqcr8VhbU0L7eV5LcZmMH.', '2018-11-05 17:13:07', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'apprenant');

-- --------------------------------------------------------

--
-- Structure de la table `vote`
--

CREATE TABLE `vote` (
  `id` int(11) NOT NULL,
  `etablissement_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nbre_etoiles` int(11) NOT NULL,
  `date_vote` date NOT NULL,
  `commentaire` longtext COLLATE utf8_unicode_ci,
  `annee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `apprenant`
--
ALTER TABLE `apprenant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C4EB462E86EC68D8` (`tuteur_id`);

--
-- Index pour la table `apprenant_exploit`
--
ALTER TABLE `apprenant_exploit`
  ADD PRIMARY KEY (`apprenant_id`,`exploit_id`),
  ADD KEY `IDX_54140632C5697D6D` (`apprenant_id`),
  ADD KEY `IDX_541406326FA4F2D4` (`exploit_id`);

--
-- Index pour la table `avis`
--
ALTER TABLE `avis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8F91ABF0FF631228` (`etablissement_id`);

--
-- Index pour la table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8F87BF96F001E734` (`code_classe`),
  ADD UNIQUE KEY `UNIQ_8F87BF961940C062` (`libelle_classe`);

--
-- Index pour la table `commentaire_exploit`
--
ALTER TABLE `commentaire_exploit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F8A5E061B5C37102` (`exploit_apprenant_id`);

--
-- Index pour la table `cursus_apprenant`
--
ALTER TABLE `cursus_apprenant`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_keys` (`annee_scolaire`,`classe`,`etablissement_apprenant_id`),
  ADD KEY `IDX_74023CF61A2EA4EC` (`etablissement_apprenant_id`),
  ADD KEY `IDX_74023CF694574E46` (`sous_classe_id`),
  ADD KEY `IDX_74023CF6AC0F2D98` (`ets_classe_niveau_specialite_id`);

--
-- Index pour la table `dirigeant`
--
ALTER TABLE `dirigeant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `domaine`
--
ALTER TABLE `domaine`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_78AF0ACCC2E9FBB6` (`libelle_domaine`);

--
-- Index pour la table `domaine_sous_domaine`
--
ALTER TABLE `domaine_sous_domaine`
  ADD PRIMARY KEY (`domaine_id`,`sous_domaine_id`),
  ADD KEY `IDX_EB8C7EEC4272FC9F` (`domaine_id`),
  ADD KEY `IDX_EB8C7EECA40AA975` (`sous_domaine_id`);

--
-- Index pour la table `enseigner`
--
ALTER TABLE `enseigner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_663E85CD94574E46` (`sous_classe_id`),
  ADD KEY `IDX_663E85CDAFD0E83D` (`etablissement_prof_id`);

--
-- Index pour la table `etablissement`
--
ALTER TABLE `etablissement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etablissement_apprenant`
--
ALTER TABLE `etablissement_apprenant`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_keys` (`etablissement_id`,`apprenant_id`),
  ADD KEY `IDX_A934CB47FF631228` (`etablissement_id`),
  ADD KEY `IDX_A934CB47C5697D6D` (`apprenant_id`);

--
-- Index pour la table `etablissement_classe`
--
ALTER TABLE `etablissement_classe`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_keys` (`etablissement_id`,`classe_id`),
  ADD KEY `IDX_45BE6910FF631228` (`etablissement_id`),
  ADD KEY `IDX_45BE69108F5EA509` (`classe_id`);

--
-- Index pour la table `etablissement_classe_niveau`
--
ALTER TABLE `etablissement_classe_niveau`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F9F4C8CEE2812860` (`etablissement_classe_id`),
  ADD KEY `IDX_F9F4C8CEFEAD13D1` (`niveau_etude_id`);

--
-- Index pour la table `etablissement_classe_niveau_specialite`
--
ALTER TABLE `etablissement_classe_niveau_specialite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3714D86DE35706DE` (`ets_classe_niveau_id`),
  ADD KEY `IDX_3714D86D2195E0F0` (`specialite_id`);

--
-- Index pour la table `etablissement_dirigeant`
--
ALTER TABLE `etablissement_dirigeant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D3189318FF631228` (`etablissement_id`),
  ADD KEY `IDX_D3189318E233AF25` (`dirigeant_id`);

--
-- Index pour la table `etablissement_prof`
--
ALTER TABLE `etablissement_prof`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9C3346ACFF631228` (`etablissement_id`),
  ADD KEY `IDX_9C3346ACABC1F7FE` (`prof_id`),
  ADD KEY `IDX_9C3346ACF46CD258` (`matiere_id`);

--
-- Index pour la table `examen`
--
ALTER TABLE `examen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_514C8FECFF631228` (`etablissement_id`);

--
-- Index pour la table `exploit`
--
ALTER TABLE `exploit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8B6D7232A40AA975` (`sous_domaine_id`);

--
-- Index pour la table `exploit_apprenant`
--
ALTER TABLE `exploit_apprenant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FADB55076FA4F2D4` (`exploit_id`),
  ADD KEY `IDX_FADB5507C5697D6D` (`apprenant_id`);

--
-- Index pour la table `fichier`
--
ALTER TABLE `fichier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9B76551FC4222328` (`chemin`),
  ADD KEY `IDX_9B76551F6FA4F2D4` (`exploit_id`),
  ADD KEY `IDX_9B76551F2EF03101` (`information_id`),
  ADD KEY `IDX_9B76551FC5697D6D` (`apprenant_id`);

--
-- Index pour la table `fondateur_ou_directeur`
--
ALTER TABLE `fondateur_ou_directeur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_29791883FF631228` (`etablissement_id`);

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9014574AAC0F2D98` (`ets_classe_niveau_specialite_id`);

--
-- Index pour la table `moyenne_periodique_apprenant`
--
ALTER TABLE `moyenne_periodique_apprenant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FAAEF32017C69EE5` (`apprenant_cursus_id`),
  ADD KEY `IDX_FAAEF320F384C1CF` (`periode_id`);

--
-- Index pour la table `niveau_etude`
--
ALTER TABLE `niveau_etude`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CFBDFA1417C69EE5` (`apprenant_cursus_id`),
  ADD KEY `IDX_CFBDFA14F46CD258` (`matiere_id`);

--
-- Index pour la table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_93C32DF3FF631228` (`etablissement_id`);

--
-- Index pour la table `prof`
--
ALTER TABLE `prof`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `programme_cours`
--
ALTER TABLE `programme_cours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CED0D14B78A3B1E0` (`enseigner_id`),
  ADD KEY `IDX_CED0D14BDC304035` (`salle_id`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4E977E5CFF631228` (`etablissement_id`);

--
-- Index pour la table `sous_classe`
--
ALTER TABLE `sous_classe`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_5F984D6116D9A4D1` (`code_sous_classe`),
  ADD UNIQUE KEY `UNIQ_5F984D61628BA66F` (`libelle_sous_classe`),
  ADD KEY `IDX_5F984D61448315C0` (`etablissement_classe_niveau_id`);

--
-- Index pour la table `sous_domaine`
--
ALTER TABLE `sous_domaine`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_5BA672816C90F4B6` (`libelle_sousdomaine`);

--
-- Index pour la table `specialite`
--
ALTER TABLE `specialite`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `stat_periodique`
--
ALTER TABLE `stat_periodique`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D5250FFE94574E46` (`sous_classe_id`);

--
-- Index pour la table `tuteur`
--
ALTER TABLE `tuteur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `uploaded_file`
--
ALTER TABLE `uploaded_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B40DF75DE76796AC` (`parent_folder_id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1D1C63B392FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_1D1C63B3A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_1D1C63B3C5697D6D` (`apprenant_id`);

--
-- Index pour la table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_keys` (`etablissement_id`,`user_id`,`annee`),
  ADD KEY `IDX_5A108564FF631228` (`etablissement_id`),
  ADD KEY `IDX_5A108564A76ED395` (`user_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `apprenant`
--
ALTER TABLE `apprenant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `avis`
--
ALTER TABLE `avis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `classe`
--
ALTER TABLE `classe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `commentaire_exploit`
--
ALTER TABLE `commentaire_exploit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cursus_apprenant`
--
ALTER TABLE `cursus_apprenant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `dirigeant`
--
ALTER TABLE `dirigeant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `domaine`
--
ALTER TABLE `domaine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `enseigner`
--
ALTER TABLE `enseigner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `etablissement`
--
ALTER TABLE `etablissement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `etablissement_apprenant`
--
ALTER TABLE `etablissement_apprenant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `etablissement_classe`
--
ALTER TABLE `etablissement_classe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `etablissement_classe_niveau`
--
ALTER TABLE `etablissement_classe_niveau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `etablissement_classe_niveau_specialite`
--
ALTER TABLE `etablissement_classe_niveau_specialite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `etablissement_dirigeant`
--
ALTER TABLE `etablissement_dirigeant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `etablissement_prof`
--
ALTER TABLE `etablissement_prof`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `examen`
--
ALTER TABLE `examen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `exploit`
--
ALTER TABLE `exploit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `exploit_apprenant`
--
ALTER TABLE `exploit_apprenant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `fichier`
--
ALTER TABLE `fichier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `fondateur_ou_directeur`
--
ALTER TABLE `fondateur_ou_directeur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `information`
--
ALTER TABLE `information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `moyenne_periodique_apprenant`
--
ALTER TABLE `moyenne_periodique_apprenant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `niveau_etude`
--
ALTER TABLE `niveau_etude`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `note`
--
ALTER TABLE `note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `prof`
--
ALTER TABLE `prof`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `programme_cours`
--
ALTER TABLE `programme_cours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `salle`
--
ALTER TABLE `salle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sous_classe`
--
ALTER TABLE `sous_classe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sous_domaine`
--
ALTER TABLE `sous_domaine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `specialite`
--
ALTER TABLE `specialite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `stat_periodique`
--
ALTER TABLE `stat_periodique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tuteur`
--
ALTER TABLE `tuteur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `uploaded_file`
--
ALTER TABLE `uploaded_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `vote`
--
ALTER TABLE `vote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `apprenant`
--
ALTER TABLE `apprenant`
  ADD CONSTRAINT `FK_C4EB462E86EC68D8` FOREIGN KEY (`tuteur_id`) REFERENCES `tuteur` (`id`);

--
-- Contraintes pour la table `apprenant_exploit`
--
ALTER TABLE `apprenant_exploit`
  ADD CONSTRAINT `FK_541406326FA4F2D4` FOREIGN KEY (`exploit_id`) REFERENCES `exploit` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_54140632C5697D6D` FOREIGN KEY (`apprenant_id`) REFERENCES `apprenant` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `avis`
--
ALTER TABLE `avis`
  ADD CONSTRAINT `FK_8F91ABF0FF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);

--
-- Contraintes pour la table `commentaire_exploit`
--
ALTER TABLE `commentaire_exploit`
  ADD CONSTRAINT `FK_F8A5E061B5C37102` FOREIGN KEY (`exploit_apprenant_id`) REFERENCES `exploit_apprenant` (`id`);

--
-- Contraintes pour la table `cursus_apprenant`
--
ALTER TABLE `cursus_apprenant`
  ADD CONSTRAINT `FK_74023CF61A2EA4EC` FOREIGN KEY (`etablissement_apprenant_id`) REFERENCES `etablissement_apprenant` (`id`),
  ADD CONSTRAINT `FK_74023CF694574E46` FOREIGN KEY (`sous_classe_id`) REFERENCES `sous_classe` (`id`),
  ADD CONSTRAINT `FK_74023CF6AC0F2D98` FOREIGN KEY (`ets_classe_niveau_specialite_id`) REFERENCES `etablissement_classe_niveau_specialite` (`id`);

--
-- Contraintes pour la table `domaine_sous_domaine`
--
ALTER TABLE `domaine_sous_domaine`
  ADD CONSTRAINT `FK_EB8C7EEC4272FC9F` FOREIGN KEY (`domaine_id`) REFERENCES `domaine` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_EB8C7EECA40AA975` FOREIGN KEY (`sous_domaine_id`) REFERENCES `sous_domaine` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `enseigner`
--
ALTER TABLE `enseigner`
  ADD CONSTRAINT `FK_663E85CD94574E46` FOREIGN KEY (`sous_classe_id`) REFERENCES `sous_classe` (`id`),
  ADD CONSTRAINT `FK_663E85CDAFD0E83D` FOREIGN KEY (`etablissement_prof_id`) REFERENCES `etablissement_prof` (`id`);

--
-- Contraintes pour la table `etablissement_apprenant`
--
ALTER TABLE `etablissement_apprenant`
  ADD CONSTRAINT `FK_A934CB47C5697D6D` FOREIGN KEY (`apprenant_id`) REFERENCES `apprenant` (`id`),
  ADD CONSTRAINT `FK_A934CB47FF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);

--
-- Contraintes pour la table `etablissement_classe`
--
ALTER TABLE `etablissement_classe`
  ADD CONSTRAINT `FK_45BE69108F5EA509` FOREIGN KEY (`classe_id`) REFERENCES `classe` (`id`),
  ADD CONSTRAINT `FK_45BE6910FF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);

--
-- Contraintes pour la table `etablissement_classe_niveau`
--
ALTER TABLE `etablissement_classe_niveau`
  ADD CONSTRAINT `FK_F9F4C8CEE2812860` FOREIGN KEY (`etablissement_classe_id`) REFERENCES `etablissement_classe` (`id`),
  ADD CONSTRAINT `FK_F9F4C8CEFEAD13D1` FOREIGN KEY (`niveau_etude_id`) REFERENCES `niveau_etude` (`id`);

--
-- Contraintes pour la table `etablissement_classe_niveau_specialite`
--
ALTER TABLE `etablissement_classe_niveau_specialite`
  ADD CONSTRAINT `FK_3714D86D2195E0F0` FOREIGN KEY (`specialite_id`) REFERENCES `specialite` (`id`),
  ADD CONSTRAINT `FK_3714D86DE35706DE` FOREIGN KEY (`ets_classe_niveau_id`) REFERENCES `etablissement_classe_niveau` (`id`);

--
-- Contraintes pour la table `etablissement_dirigeant`
--
ALTER TABLE `etablissement_dirigeant`
  ADD CONSTRAINT `FK_D3189318E233AF25` FOREIGN KEY (`dirigeant_id`) REFERENCES `dirigeant` (`id`),
  ADD CONSTRAINT `FK_D3189318FF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);

--
-- Contraintes pour la table `etablissement_prof`
--
ALTER TABLE `etablissement_prof`
  ADD CONSTRAINT `FK_9C3346ACABC1F7FE` FOREIGN KEY (`prof_id`) REFERENCES `prof` (`id`),
  ADD CONSTRAINT `FK_9C3346ACF46CD258` FOREIGN KEY (`matiere_id`) REFERENCES `matiere` (`id`),
  ADD CONSTRAINT `FK_9C3346ACFF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);

--
-- Contraintes pour la table `examen`
--
ALTER TABLE `examen`
  ADD CONSTRAINT `FK_514C8FECFF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);

--
-- Contraintes pour la table `exploit`
--
ALTER TABLE `exploit`
  ADD CONSTRAINT `FK_8B6D7232A40AA975` FOREIGN KEY (`sous_domaine_id`) REFERENCES `sous_domaine` (`id`);

--
-- Contraintes pour la table `exploit_apprenant`
--
ALTER TABLE `exploit_apprenant`
  ADD CONSTRAINT `FK_FADB55076FA4F2D4` FOREIGN KEY (`exploit_id`) REFERENCES `exploit` (`id`),
  ADD CONSTRAINT `FK_FADB5507C5697D6D` FOREIGN KEY (`apprenant_id`) REFERENCES `apprenant` (`id`);

--
-- Contraintes pour la table `fichier`
--
ALTER TABLE `fichier`
  ADD CONSTRAINT `FK_9B76551F2EF03101` FOREIGN KEY (`information_id`) REFERENCES `information` (`id`),
  ADD CONSTRAINT `FK_9B76551F6FA4F2D4` FOREIGN KEY (`exploit_id`) REFERENCES `exploit` (`id`),
  ADD CONSTRAINT `FK_9B76551FC5697D6D` FOREIGN KEY (`apprenant_id`) REFERENCES `apprenant` (`id`);

--
-- Contraintes pour la table `information`
--
ALTER TABLE `information`
  ADD CONSTRAINT `FK_29791883FF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);

--
-- Contraintes pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD CONSTRAINT `FK_9014574AAC0F2D98` FOREIGN KEY (`ets_classe_niveau_specialite_id`) REFERENCES `etablissement_classe_niveau_specialite` (`id`);

--
-- Contraintes pour la table `moyenne_periodique_apprenant`
--
ALTER TABLE `moyenne_periodique_apprenant`
  ADD CONSTRAINT `FK_FAAEF32017C69EE5` FOREIGN KEY (`apprenant_cursus_id`) REFERENCES `cursus_apprenant` (`id`),
  ADD CONSTRAINT `FK_FAAEF320F384C1CF` FOREIGN KEY (`periode_id`) REFERENCES `periode` (`id`);

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `FK_CFBDFA1417C69EE5` FOREIGN KEY (`apprenant_cursus_id`) REFERENCES `cursus_apprenant` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14F46CD258` FOREIGN KEY (`matiere_id`) REFERENCES `matiere` (`id`);

--
-- Contraintes pour la table `periode`
--
ALTER TABLE `periode`
  ADD CONSTRAINT `FK_93C32DF3FF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);

--
-- Contraintes pour la table `programme_cours`
--
ALTER TABLE `programme_cours`
  ADD CONSTRAINT `FK_CED0D14B78A3B1E0` FOREIGN KEY (`enseigner_id`) REFERENCES `enseigner` (`id`),
  ADD CONSTRAINT `FK_CED0D14BDC304035` FOREIGN KEY (`salle_id`) REFERENCES `salle` (`id`);

--
-- Contraintes pour la table `salle`
--
ALTER TABLE `salle`
  ADD CONSTRAINT `FK_4E977E5CFF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);

--
-- Contraintes pour la table `sous_classe`
--
ALTER TABLE `sous_classe`
  ADD CONSTRAINT `FK_5F984D61448315C0` FOREIGN KEY (`etablissement_classe_niveau_id`) REFERENCES `etablissement_classe_niveau` (`id`);

--
-- Contraintes pour la table `stat_periodique`
--
ALTER TABLE `stat_periodique`
  ADD CONSTRAINT `FK_D5250FFE94574E46` FOREIGN KEY (`sous_classe_id`) REFERENCES `sous_classe` (`id`);

--
-- Contraintes pour la table `uploaded_file`
--
ALTER TABLE `uploaded_file`
  ADD CONSTRAINT `FK_B40DF75DE76796AC` FOREIGN KEY (`parent_folder_id`) REFERENCES `uploaded_file` (`id`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `FK_1D1C63B3C5697D6D` FOREIGN KEY (`apprenant_id`) REFERENCES `apprenant` (`id`);

--
-- Contraintes pour la table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `FK_5A108564A76ED395` FOREIGN KEY (`user_id`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `FK_5A108564FF631228` FOREIGN KEY (`etablissement_id`) REFERENCES `etablissement` (`id`);
--
-- Base de données :  `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Structure de la table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Structure de la table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Structure de la table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Structure de la table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Structure de la table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Structure de la table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Structure de la table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Contenu de la table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"QuickFly\",\"table\":\"vol\"},{\"db\":\"QuickFly\",\"table\":\"reservation\"},{\"db\":\"QuickFly\",\"table\":\"user\"},{\"db\":\"QuickFly\",\"table\":\"avion\"},{\"db\":\"QuickFly\",\"table\":\"Bagage\"},{\"db\":\"QuickFly\",\"table\":\"client\"},{\"db\":\"QuickFly\",\"table\":\"admin\"},{\"db\":\"\",\"table\":\"client\"},{\"db\":\"db_billetavion\",\"table\":\"utilisateur\"},{\"db\":\"db_billetavion\",\"table\":\"compagnie\"}]');

-- --------------------------------------------------------

--
-- Structure de la table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Structure de la table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Structure de la table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Structure de la table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Structure de la table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Contenu de la table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2019-02-24 20:04:32', '{\"lang\":\"fr\",\"collation_connection\":\"utf8mb4_unicode_ci\",\"SendErrorReports\":\"always\"}');

-- --------------------------------------------------------

--
-- Structure de la table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Structure de la table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Index pour les tables exportées
--

--
-- Index pour la table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Index pour la table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Index pour la table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Index pour la table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Index pour la table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Index pour la table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Index pour la table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Index pour la table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Index pour la table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Index pour la table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Index pour la table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Index pour la table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Index pour la table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Index pour la table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Base de données :  `sogo`
--
CREATE DATABASE IF NOT EXISTS `sogo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `sogo`;

-- --------------------------------------------------------

--
-- Structure de la table `sogo_users`
--

CREATE TABLE `sogo_users` (
  `c_uid` varchar(10) NOT NULL,
  `c_name` varchar(10) DEFAULT NULL,
  `c_password` varchar(32) DEFAULT NULL,
  `c_cn` varchar(128) DEFAULT NULL,
  `mail` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `sogo_users`
--

INSERT INTO `sogo_users` (`c_uid`, `c_name`, `c_password`, `c_cn`, `mail`) VALUES
('sidoine', 'ODE Sidoin', 'dfbb885c6e4743f30025399a97c65ab0', 'ODE Sidoine', 'sidoine.ode@imsp-uac.org'),
('sogo', 'SOGo Admin', 'dfbb885c6e4743f30025399a97c65ab0', 'SOGo Administrator', 'osidoine@gmail.com');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `sogo_users`
--
ALTER TABLE `sogo_users`
  ADD PRIMARY KEY (`c_uid`);
--
-- Base de données :  `springboot_mariadb`
--
CREATE DATABASE IF NOT EXISTS `springboot_mariadb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `springboot_mariadb`;

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `age` int(11) NOT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `sexe` char(1) NOT NULL,
  `tel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
