/* PROJECT ARCHI-J2E  */

/* CREATION DES TABLES*/


create table user (
id int(6) primary key auto_increment,
typeUser  varchar(10) not null default "client",
mail varchar (255) not null,
mdp varchar (255) not null
);

create table avion(
id int(4) primary key auto_increment,
nom varchar(255),
compagnie varchar(255),
categorie varchar (30),
typeAvion varchar (30),
place int (3),
poidsMax double,
surplus boolean
);

create table Bagage(
id int(5) primary key auto_increment, 
typeB varchar (30) not null,
poidsB double not null,
idUser int(6),
foreign key (idUser) references user(id)
);


create table vol(
id int(6) primary key auto_increment,
villeD varchar(255) not null,
villeA varchar(255) not null,
dateD DATE not null,
dateA DATE not null,
aeroportD varchar(30) not null,
aeroportA varchar(30) not null,
prix double not null,
tempsEscale varchar(5),
idAvion int(4) not null,
foreign key (idAvion) REFERENCES avion(id)
);

create table reservation (
id int(8) primary key auto_increment,
idUser int (6) not null,
idVol int (6) not null,
foreign key (idVol) references vol(id),
foreign key (idUser) references user(id)
);



/*INSETION DES CLIENTS*/

INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'emma@gmail.com','imsp123');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'christian@gmail.com','imsp12');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'yao@gmail.com','imsp0');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'ode@gmail.com','imsp1');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'imsp1@gmail.com','imsp1');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp2@gmail.com','imsp2');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp3@gmail.com','imsp3');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'imsp4gmail.com','imsp4');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp5@gmail.com','imsp5');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp6@gmail.com','imsp6');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'imsp7@gmail.com','imsp7');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp8@gmail.com','imsp8');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'imsp9@gmail.com','imsp9');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp10@gmail.com','imsp10');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp11@gmail.com','imsp11');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'imsp12@gmail.com','imsp12');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp13@gmail.com','imsp13');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp14@gmail.com','imsp14');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'imsp15@gmail.com','imsp15');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp16@gmail.com','imsp16');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'imsp17@gmail.com','imsp17');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp18@gmail.com','imsp18');
INSERT INTO user(typeUser,mail,mdp)VALUES('client', 'imsp19@gmail.com','imsp19');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'imsp20@gmail.com','imsp20');
INSERT INTO user(typeUser,mail,mdp) VALUES('client', 'imsp21@gmail.com','imsp21');

 /* INSERTIONS ADMIN */

INSERT INTO user(typeUser,mail,mdp) VALUES('admin', 'sidoine.ode@imsp-uac.org','66091913');

/* INSERTIONS DES AVIONS */

INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('kq', 'kenyairways', 'AirLourd', 'AirPressurise', 500, 65,1);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('rwdair', 'rwandair_express', 'AirLeger', 'AirCargo', 200, 23.5, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('ethair', 'ethiopian_airlines', 'AirMoyen', 'AirRoulant', 300, 42, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('zenair', 'zenirair','AirLourd','AirVivant', 320, 65, 1);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('coair', 'trans_air_congo', 'AirLourd', 'AirVivant', 450, 65, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('sgair', 'senegal_airlines', 'AirMoyen', 'AirCargo', 280, 42, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('ngair', 'niger_ailines', 'AirLeger', 'AirRoulant', 130, 23.5, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('ngriair', 'nigeria_airlines', 'AirMoyen', 'AirCargo', 250, 42, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('algair', 'algerie_airlines', 'AirLourd', 'AirRoulant', 480, 65,0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('shair', 'sahara_airlines', 'AirMoyen', 'AirVivant', 330, 42, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('exair', 'air_excel', 'AirMoyen', 'AirCargo', 270, 42, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('tzair', 'air_tanzania', 'AirLourd', 'AirVivant', 440, 65, 1);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('salair', 'as_salaam_air', 'AirLeger', 'AirCargo', 165, 23.5, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('arcair', 'auric_air', 'AirMoyen', 'AirRoulant', 290, 42, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('ctlair', 'costal_aviation', 'AirLeger', 'AirPressurise', 165, 23.5, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('fsair', 'fastjet_tanzania', 'AirLourd', 'AirVivant', 490, 65, 1);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('prcsair', 'precision_air', 'AirMoyen', 'AirVivant', 355, 42, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('kens', 'kenyairways', 'AirLourd', 'AirRoulant', 5000, 50, 42);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('titus', 'coastal_aviation', 'AirLeger', 'AirCargo', 200, 18, 1);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('sumas', 'air_tanzania', 'AirLourd', 'AirVivant', 600, 70, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('curstos', 'sahara_airlines', 'AirMoyen', 'AirPressurise', 250, 35, 1);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('naja', 'nigeria_airlines', 'AirMoyen', 'AirRoulant', 195, 25, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('narech', 'algerie_airlines', 'AirLourd', 'AirCargo', 480, 60, 0);

INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('powerful', 'air_excel', 'AirLeger', 'AirVivant', 140, 45, 1);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('guide', 'safari_plus', 'AirLeger', 'AirRoulant', 325, 13, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('tor', 'zenith_air', 'AirLourd', 'Airvivant', 320, 64, 1);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('bonam', 'gouanzou_airlines', 'AirMoyen', 'AirCargo', 450, 60, 0);
INSERT INTO avion (nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) VALUES ('wolkork', 'senegal_airlines', 'AirLeger', 'AirRoullant', 135, 20, 1);


/* INSERTION DES VOLS */

INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('bujumbura', 'nairobi', '2019-4-1', '2019-4-1', 'bujumbura', 'nairobi', 320000, 3, 1);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('nairobi', 'cotonou', '2019-4-1', '2019-4-2', 'nairobi', 'cotonou', 500000, 0, 1);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('bujumbora', 'cotonou', '2019-4-3', '2019-4-4', 'bujumbora', 'cotonou', 820000, 0, 4);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('cotonou', 'bujumbura', '2019-4-4', '2019-4-5', 'cotonou', 'bujumbura', 150000, 0, 4);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('cotonou', 'nairobi', '2019-4-5', '2019-4-5', 'cotonou', 'nairobi', 500000, 0, 8);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('nairobi', 'bujumbura', '2019-4-5', '2019-4-6', 'nairobi', 'bujumbura', 300000, 0, 8);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('abuja', 'nairobi', '2019-4-7', '2019-4-7', 'abuja', 'nairobi', 180000, 4, 8);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('nairobi', 'bujumbura', '2019-4-7', '2019-4-8', 'nairobi', 'bujumbura', 300000, 0, 2);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('casablanca', 'nairobi', '2019-4-9', '2019-4-10', 'casablanca', 'nairobi', 250000, 5, 10);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('nairobi', 'bujumbura', '2019-4-10', '2019-4-10', 'nairobi', 'bujumbura', 250000, 0, 11);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('lagos', 'cotonou', '2019-4-11', '2019-4-11', 'lagos', 'cotonou', 280000, 0, 18);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('lagos', 'nairobi', '2019-4-12', '2019-4-13', 'lagos', 'nairobi', 350000, 5, 4);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('nairobi', 'bujumbura', '2019-4-13', '2019-4-13', 'nairobi', 'bujumbura', 200000, 0, 12);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('caire', 'cotonou', '2019-4-14', '2019-4-14', 'caire', 'cotonou', 300000, 0, 14);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('addis abeba', 'nairobi', '2019-4-15','2019-4-15', 'addis abeba', 'nairobi', 350000, 2, 9);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('nairobi', 'dar-es-salam', '2019-4-15', '2019-4-16', 'nairobi', 'dar-es-salam', 700000, 0, 13);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('kigali', 'cotonou', '2019-4-17', '2019-4-17', 'kigali', 'cotonou', 500000, 0, 2);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('kigali', 'bujumbura', '2019-4-18', '2019-4-18', 'kigali', 'bujumbura', 300000, 0, 5);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('brazaville', 'bamako', '2019-4-20', '2019-4-20', 'brazaville', 'bamako', 150000, 0,20);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('marakech', 'dakar', '2019-4-21', '2019-4-21', 'marakech', 'dakar', 350000, 3,25);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('addis abeba', 'gounanzou', '2019-4-22', '2019-4-23', 'addis abeba', 'gouanzou', 700000, 7, 2);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('addis abeba', 'gounanzou', '2019-4-22', '2019-4-23', 'addis abeba', 'gouanzou', 700000, 7, 2);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('libreville', 'kigali', '2019-4-23', '2019-4-23', 'libreville', 'kigali', 320000, 0,5);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('abuja', 'abidjan', '2019-4-24', '2019-4-24', 'abuja', 'abijan', 200000, 0,8);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('dakar', 'cotonou', '2019-4-21', '2019-4-22', 'dakar', 'cotonou', 250000, 0,3);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('cotonou', 'addis abeba', '2019-4-21', '2019-4-22', 'cotonou', 'addis abeba', 400000, 4,23);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('addis abeba', 'gounanzou', '2019-4-22', '2019-4-23', 'addis abeba', 'gouanzou', 700000, 7,20);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('gounanzou', 'addis abeba', '2019-4-24', '2019-4-25', 'gounazou', 'addis abeba', 500000, 7,18);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('dakar', 'abidjan', '2019-4-21', '2019-4-21', 'dakar', 'abidjan', 230000, 3,11);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('abidjan', 'cotonou', '2019-4-21', '2019-4-22', 'abibjan', 'cotonou', 200000, 0,21);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('cotonou', 'dakar', '2019-4-23', '2019-4-24', 'cotonou', 'dakar', 230000, 0,6);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('kigali', 'libreville', '2019-4-23', '2019-4-24', 'kigali', 'libreville', 300000, 0,19);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('ouagadougou', 'niamey', '2019-4-25', '2019-4-25', 'ouagadougou', 'niamey', 120000, 2,7);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('niamey', 'cotonou', '2019-4-25', '2019-4-25', 'niamey', 'cotonou', 85000, 0,7);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('cotonou', 'accra', '2019-4-26', '2019-4-26', 'cotonou', 'accra', 100000, 4,10);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('accra', 'libreville', '2019-4-26', '2019-4-27', 'accra', 'libreville', 240000, 6,5);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('libreville', 'njamena', '2019-4-27', '2019-4-28', 'libreville', 'njamena', 350000, 3,14);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('njamena', 'libreville', '2019-4-27', '2019-4-28', 'njamena', 'libreville', 350000, 3,14);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('libreville', 'accra', '2019-4-29', '2019-4-30', 'libreville', 'accra', 240000, 6,17);
INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) VALUES('accra', 'cotonou', '2019-4-30', '2019-5-1', 'accra', 'cotonou', 100000, 0,8);




INSERT INTO reservation (idUser,idVol)VALUES (15,1);
INSERT INTO reservation (idUser,idVol)VALUES (6,14 );
INSERT INTO reservation (idUser,idVol)VALUES (10, 29);
INSERT INTO reservation (idUser,idVol)VALUES (14, 12);
INSERT INTO reservation (idUser,idVol)VALUES (13, 5);
INSERT INTO reservation (idUser,idVol)VALUES (9,11);
INSERT INTO reservation (idUser,idVol)VALUES (1, 18);
INSERT INTO reservation (idUser,idVol)VALUES (4, 2);
INSERT INTO reservation (idUser,idVol)VALUES (21,7);
INSERT INTO reservation (idUser,idVol)VALUES (11, 19);
INSERT INTO reservation (idUser,idVol)VALUES (17, 4);
INSERT INTO reservation (idUser,idVol)VALUES (20, 6);
INSERT INTO reservation (idUser,idVol)VALUES (12, 9);
INSERT INTO reservation (idUser,idVol)VALUES (16, 13);
INSERT INTO reservation (idUser,idVol)VALUES (7, 25);
INSERT INTO reservation (idUser,idVol)VALUES (5, 28);
INSERT INTO reservation (idUser,idVol)VALUES (2, 26);
INSERT INTO reservation (idUser,idVol)VALUES (14, 4);
INSERT INTO reservation (idUser,idVol)VALUES (25, 2);
INSERT INTO reservation (idUser,idVol)VALUES (23, 8);
INSERT INTO reservation (idUser,idVol)VALUES (1, 5);
INSERT INTO reservation (idUser,idVol)VALUES (5, 1);
INSERT INTO reservation (idUser,idVol)VALUES (4, 17);
INSERT INTO reservation (idUser,idVol)VALUES (9, 4);
INSERT INTO reservation (idUser,idVol)VALUES (17,21);




