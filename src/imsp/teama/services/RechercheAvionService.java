package imsp.teama.services;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.repository.AvionRepository;

/**
 * Servlet implementation class RechercheAvionService
 */
@WebServlet("/RechercheAvionService")
public class RechercheAvionService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RechercheAvionService() {
        super();
    }

private AvionRepository avionRepository;
 	
 	public void init()  throws ServletException{
 		DaoFactory daoFactory = DaoFactory.getInstance();
 		this.avionRepository = daoFactory.getAvionRepository();
 	}
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("categories",avionRepository.ListAttributAvion("categorie"));
		request.setAttribute("typesAvion",avionRepository.ListAttributAvion("typeAvion"));
		this.getServletContext().getRequestDispatcher("/WEB-INF/RechercheAvion.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String categorie = request.getParameter("categorie");
		String typeAvion = request.getParameter("typeAvion");
		String operation = request.getParameter("operation");
		
		if(operation.equalsIgnoreCase("configuration"))
		{
		int id = Integer.parseInt(request.getParameter("idAvion"));
		double poidsMax = Double.parseDouble(request.getParameter("poidsMax"));
		avionRepository.ConfigurerAvion(id, poidsMax);
		}
		
		if(categorie.equalsIgnoreCase("tous"))
		{ 
			if(typeAvion.equalsIgnoreCase("tous"))
			{
				request.setAttribute("avions",avionRepository.ListAvion());
			}
			else
			{
				request.setAttribute("avions",avionRepository.ListAvionParType(typeAvion));
			}
		}
		else
		{
			if(typeAvion.equalsIgnoreCase("tous"))
			{
				request.setAttribute("avions",avionRepository.ListAvionParCategorie(categorie));
			}
			else
			{
				request.setAttribute("avions",avionRepository.ListAvionParCategorieOuType(categorie,typeAvion));
			}
		
		}
		
		request.setAttribute("message","Essayer une autre recherche ?");
		request.setAttribute("categorie", categorie);
		request.setAttribute("typeAvion", typeAvion);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/ConfigurerAvionsRecherches.jsp").forward(request, response);
	
		
	}
	
}
