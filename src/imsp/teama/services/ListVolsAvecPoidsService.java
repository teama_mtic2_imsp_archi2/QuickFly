package imsp.teama.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.entities.VolAvion;
import imsp.teama.repository.VolRepository;

/**
 * Servlet implementation class ListVolsAvecPoidsService
 */
@WebServlet("/ListVolsAvecPoidsService")
public class ListVolsAvecPoidsService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListVolsAvecPoidsService() {
        super();
    }
private VolRepository volRepository;
 	
 	public void init()  throws ServletException{
 		DaoFactory daoFactory = DaoFactory.getInstance();
 		this.volRepository = daoFactory.getVolRepository();
 	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("villesD", volRepository.getVilleOuAeroport("villeD"));
		request.setAttribute("villesA", volRepository.getVilleOuAeroport("villeA"));
		this.getServletContext().getRequestDispatcher("/WEB-INF/ListVolsAvecPoids.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String villeD = request.getParameter("villeD");
		String villeA = request.getParameter("villeA");
		String dateD = request.getParameter("dateD");
		double poidsB = Double.parseDouble(request.getParameter("poidsB"));
		
		boolean volR = Boolean.parseBoolean(request.getParameter("volR"));
		boolean volE = Boolean.parseBoolean(request.getParameter("volE"));
		
		
		if(villeD.contentEquals(villeA))
		{
			request.setAttribute("villesD", volRepository.getVilleOuAeroport("villeD"));
			request.setAttribute("villesA", volRepository.getVilleOuAeroport("villeA"));
			request.setAttribute("error", "Ville d'arrivée identique à ville de départ !");
			this.getServletContext().getRequestDispatcher("/WEB-INF/ListVolsAvecPoids.jsp").forward(request, response);
			
		}
		else
		{
			
			List <VolAvion> volsR = new ArrayList<VolAvion>();
			List <VolAvion> volsEA = new ArrayList<VolAvion>();
			List <VolAvion> volsER = new ArrayList<VolAvion>();
		
			String link = "ListVolsAvecPoids";
			request.setAttribute("link",link);
			if(dateD.equals(null))
			{
				Date date = new Date();
				dateD = date.getYear()+"-"+date.getMonth()+"-"+date.getDate();
			}
			
			//vol direct allée
			List <VolAvion> volsA = volRepository.ListVolsAvecPoids(villeD,villeA,dateD,poidsB);
			
			if(volsA.isEmpty()) {
				request.setAttribute("messageA","Pas de vol direct allée !");
				}
			else {
				request.setAttribute("volsA",volsA);
				}
			
	
			//vol escale allée 
			if(volE || volsA.isEmpty())
			{
				
				 volsEA = volRepository.getVolsEscaleAvecPoids(villeD, villeA, dateD, poidsB);
				
				if(volsEA.isEmpty()) {
					request.setAttribute("messageEA","Pas de vol escale allée !");
					}
				else {
					request.setAttribute("volsEA",volsEA);
					}
			
			}
			
			
			//vol retour direct
			if(volR) 
			{
					if(!volsA.isEmpty() || !volsEA.isEmpty() ) 
					{
						
						if(!volsA.isEmpty())
						{
							for (VolAvion vol : volsA) 
							{
								
								List <VolAvion>  vols = volRepository.ListVolsAvecPoids(villeA,villeD,vol.getDateA(),poidsB);
								
								if(!vols.isEmpty())
								{
								volsR.addAll(vols);
									
								}
							}
						}
						else
						{
							volsR = volRepository.ListVolsAvecPoids(villeA,villeD,dateD,poidsB);
						}
							
						if(volsR.isEmpty()) 
						{
							request.setAttribute("messageR","Pas de vol direct retour  !");
						}
						else 
						{
							request.setAttribute("volsR",volsR);
						}
				  }
		
			}
			
			
			//vol escale retour 
			if(volR ) 
			{
				if(!volsA.isEmpty() || !volsEA.isEmpty() ) 
				{
					if(!volsA.isEmpty())
					{
						for (VolAvion vol : volsA) {
							
							List <VolAvion>  vols = volRepository.getVolsEscaleAvecPoids(villeA,villeD,vol.getDateA(),poidsB);
							
							if(!vols.isEmpty()) {
							volsER.addAll(vols);
								
							}
						}
					}
					else 
					{
						volsER = volRepository.getVolsEscaleAvecPoids(villeA,villeD,dateD,poidsB);	
					}
						
					if(volsER.isEmpty() && volE || volsR.isEmpty()) {
						request.setAttribute("messageER","Pas de vol escale retour !");
					}
					else {
						request.setAttribute("volsER",volsER);
					}
			  }
			}
			
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/ResultatRecherche.jsp").forward(request, response);
		}
			
		
	}

}
