package imsp.teama.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.entities.Client;
import imsp.teama.entities.User;
import imsp.teama.repository.UserRepository;

/**
 * Servlet implementation class RegisterService
 */
@WebServlet("/RegisterService")
public class RegisterService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserRepository userRepository;
	
	public void init()  throws ServletException{
		DaoFactory daoFactory = DaoFactory.getInstance();
		this.userRepository = daoFactory.getUserRepository();
	}
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//request.setAttribute("users", userRepository.lister());
		this.getServletContext().getRequestDispatcher("/WEB-INF/Register.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mail = request.getParameter("mail");
		String mdp = request.getParameter("mdp");
		String mdpbis = request.getParameter("mdpbis");
		
		
		if(mail.equalsIgnoreCase("")) {
			String error = "";
			error = "Veillez remplir tous les champs ! ";
			request.setAttribute("error", error);
			this.getServletContext().getRequestDispatcher("/WEB-INF/Register.jsp").forward(request, response);
		}
		
		if(mdp.equalsIgnoreCase("") || mdpbis.equalsIgnoreCase("")) {
			String error = "";
			error = "Veillez remplir tous les champs ! ";
			request.setAttribute("error", error);
			this.getServletContext().getRequestDispatcher("/WEB-INF/Register.jsp").forward(request, response);
		}
		
		if(!mdp.equals(mdpbis))
		{
			String error = "";
			error = "Les mots de passe ne sont pas identiques ! ";
			request.setAttribute("error", error);
			this.getServletContext().getRequestDispatcher("/WEB-INF/Register.jsp").forward(request, response);
		}
		
			if(userRepository.checkMail(mail)==true)
			{
				String error = "";
				error = "Mail déjà associé à un autre compte, essayer un autre !";
				request.setAttribute("error", error);
				this.getServletContext().getRequestDispatcher("/WEB-INF/Register.jsp").forward(request, response);
			}
			else {
				User user = new Client();
				user.setTypeUser("client");
				user.setMail(mail);
				user.setMdp(mdp);
				if(userRepository.ajouter(user)) {
				response.sendRedirect("Login");
				}
			}
	}

}
