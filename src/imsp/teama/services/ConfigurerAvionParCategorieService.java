package imsp.teama.services;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.repository.AvionRepository;

/**
 * Servlet implementation class ConfigurerAvionParCategorieService
 */
@WebServlet("/ConfigurerAvionParCategorieService")
public class ConfigurerAvionParCategorieService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ConfigurerAvionParCategorieService() {
        super();
        // TODO Auto-generated constructor stub
    }
    
 private AvionRepository avionRepository;
 	
 	public void init()  throws ServletException{
 		DaoFactory daoFactory = DaoFactory.getInstance();
 		this.avionRepository = daoFactory.getAvionRepository();
 	}
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("avions",avionRepository.ListAvionParCategorie("AirLourd"));
		this.getServletContext().getRequestDispatcher("/WEB-INF/ConfigurerAvionParCategorie.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int id = Integer.parseInt(request.getParameter("idAvion"));
		double poidsMax = Double.parseDouble(request.getParameter("poidsMax"));
		if(avionRepository.ConfigurerAvion(id, poidsMax))
		{
			request.setAttribute("avions",avionRepository.ListAvionParCategorie("AirLourd"));
			this.getServletContext().getRequestDispatcher("/WEB-INF/ConfigurerAvionParCategorie.jsp").forward(request, response);
		}
	}

}
