package imsp.teama.services;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.entities.Vol;
import imsp.teama.repository.VolRepository;

/**
 * Servlet implementation class AjouterVolService
 */
@WebServlet("/AjouterVolService")
public class AjouterVolService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VolRepository volRepository;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjouterVolService() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init()  throws ServletException{
 		DaoFactory daoFactory = DaoFactory.getInstance();
 		this.volRepository = daoFactory.getVolRepository();
 	}
    
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/AjouterVol.jsp").forward(request, response);
	}

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String villeD = request.getParameter("villeD");
		String villeA = request.getParameter("villeA");
		String dateD = request.getParameter("dateD");
		String dateA = request.getParameter("dateA");
		String aeroportD = request.getParameter("aeroportD");
		String aeroportA = request.getParameter("aeroportA");
		double prix = Double.parseDouble(request.getParameter("prix"));
		int tempsEscale = Integer.parseInt(request.getParameter("tempsEscale"));
		int idAvion = Integer.parseInt(request.getParameter("idAvion"));
		
		Vol vol = new Vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion);
		
		if(volRepository.AjouterVol(vol))
		{
			String succes="Vol ajouté";
			String link = "AjouterVol";
			String message = "Ajouter un autre vol ?";
			request.setAttribute("succes",succes);
			request.setAttribute("link",link);
			request.setAttribute("message",message);
			this.getServletContext().getRequestDispatcher("/WEB-INF/ResultatAjout.jsp").forward(request, response);
		}
		else {
			String error = "Une erreur est souvenue, veillez réessayer !";
			String link = "AjouterVol";
			String message = "Ajouter un autre vol ?";
			request.setAttribute("link",link);
			request.setAttribute("error",error);
			request.setAttribute("message",message);
			this.getServletContext().getRequestDispatcher("/WEB-INF/ResultatAjout.jsp").forward(request, response);
		}
		
	}

}
