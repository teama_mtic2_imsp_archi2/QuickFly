package imsp.teama.services;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.repository.VolRepository;

/**
 * Servlet implementation class ListVolService
 */
@WebServlet("/ListVolService")
public class ListVolService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListVolService() {
        super();
        // TODO Auto-generated constructor stub
    }
    private VolRepository volRepository;
 	
 	public void init()  throws ServletException{
 		DaoFactory daoFactory = DaoFactory.getInstance();
 		this.volRepository = daoFactory.getVolRepository();
 	}
 	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("vols",volRepository.ListVol());
		this.getServletContext().getRequestDispatcher("/WEB-INF/ListVol.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
