package imsp.teama.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.entities.VolAvion;
import imsp.teama.repository.AvionRepository;
import imsp.teama.repository.VolRepository;

/**
 * Servlet implementation class ListVolSelonTypeBagageService
 */
@WebServlet("/ListVolSelonTypeBagageService")
public class ListVolSelonTypeBagageService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListVolSelonTypeBagageService() {
        super();
    }
    private VolRepository volRepository;
    private AvionRepository avionRepository;
 	
 	public void init()  throws ServletException{
 		DaoFactory daoFactory = DaoFactory.getInstance();
 		this.volRepository = daoFactory.getVolRepository();
 		this.avionRepository = daoFactory.getAvionRepository();
 	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("villesD", volRepository.getVilleOuAeroport("villeD"));
		request.setAttribute("villesA", volRepository.getVilleOuAeroport("villeA"));
		request.setAttribute("typesB", avionRepository.ListAttributAvion("typeAvion"));
		this.getServletContext().getRequestDispatcher("/WEB-INF/ListVolSelonTypeBagage.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String villeD = request.getParameter("villeD");
		String villeA = request.getParameter("villeA");
		String dateD = request.getParameter("dateD");
		double poidsB = Double.parseDouble(request.getParameter("poidsB"));
		String typeB = request.getParameter("typeB");
		
		boolean volR = Boolean.parseBoolean(request.getParameter("volR"));
		boolean volE = Boolean.parseBoolean(request.getParameter("volE"));
		
		
		

		if(villeD.contentEquals(villeA))
		{
			request.setAttribute("villesD", volRepository.getVilleOuAeroport("villeD"));
			request.setAttribute("villesA", volRepository.getVilleOuAeroport("villeA"));
			request.setAttribute("error", "Ville d'arrivée identique à ville de départ !");
			this.getServletContext().getRequestDispatcher("/WEB-INF/ListVolSelonTypeBagage.jsp").forward(request, response);
			
		}
		else
		{
			
			List <VolAvion> volsR = new ArrayList<VolAvion>();
			List <VolAvion> volsEA = new ArrayList<VolAvion>();
			List <VolAvion> volsER = new ArrayList<VolAvion>();
		
			String link = "ListVolSelonTypeBagage";
			request.setAttribute("link",link);
			if(dateD.equals(null))
			{
				Date date = new Date();
				dateD = date.getYear()+"-"+date.getMonth()+"-"+date.getDate();
			}
			
			//vol direct allée
			List <VolAvion> volsA = volRepository.ListVolSelonTypeBagage(villeD,villeA,dateD,poidsB,typeB);
			
			if(volsA.isEmpty()) {
				request.setAttribute("messageA","Pas de vol direct allée !");
				}
			else {
				request.setAttribute("volsA",volsA);
				}
			
	
			//vol escale allée 
			if(volE || volsA.isEmpty())
			{
				
				 volsEA = volRepository.getVolsEscaleSelonTypeBagage(villeD, villeA, dateD, poidsB, typeB);
				
				if(volsEA.isEmpty()) {
					request.setAttribute("messageEA","Pas de vol escale allée !");
					}
				else {
					request.setAttribute("volsEA",volsEA);
					}
			
			}
			
			
			//vol retour direct
			if(volR) 
			{
					if(!volsA.isEmpty() || !volsEA.isEmpty() ) 
					{
						
						if(!volsA.isEmpty())
						{
							for (VolAvion vol : volsA) 
							{
								
								List <VolAvion>  vols = volRepository.ListVolSelonTypeBagage(villeA,villeD,vol.getDateA(),poidsB,typeB);
								
								if(!vols.isEmpty())
								{
								volsR.addAll(vols);
									
								}
							}
						}
						else
						{
							volsR = volRepository.ListVolSelonTypeBagage(villeA,villeD,dateD,poidsB,typeB);
						}
							
						if(volsR.isEmpty()) 
						{
							request.setAttribute("messageR","Pas de vol direct retour  !");
						}
						else 
						{
							request.setAttribute("volsR",volsR);
						}
				  }
		
			}
			
			
			//vol escale retour 
			if(volR ) 
			{
				if(!volsA.isEmpty() || !volsEA.isEmpty() ) 
				{
					if(!volsA.isEmpty())
					{
						for (VolAvion vol : volsA) {
							
							List <VolAvion>  vols = volRepository.getVolsEscaleSelonTypeBagage(villeA,villeD,vol.getDateA(),poidsB,typeB);
							
							if(!vols.isEmpty()) {
							volsER.addAll(vols);
								
							}
						}
					}
					else 
					{
						volsER = volRepository.getVolsEscaleSelonTypeBagage(villeA,villeD,dateD,poidsB,typeB);	
					}
						
					if(volsER.isEmpty() && volE || volsR.isEmpty()) {
						request.setAttribute("messageER","Pas de vol escale retour !");
					}
					else {
						request.setAttribute("volsER",volsER);
					}
			  }
			}
			
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/ResultatRecherche.jsp").forward(request, response);
		}
			
	}
}
