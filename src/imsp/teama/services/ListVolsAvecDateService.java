package imsp.teama.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.entities.VolAvion;
import imsp.teama.repository.VolRepository;


@WebServlet("/ListVolsAvecDateService")
public class ListVolsAvecDateService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    public ListVolsAvecDateService() {
        super();
    }
    
    
 	private VolRepository volRepository;
 	
 	public void init()  throws ServletException{
 		DaoFactory daoFactory = DaoFactory.getInstance();
 		this.volRepository = daoFactory.getVolRepository();
 	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("villesD", volRepository.getVilleOuAeroport("villeD"));
		request.setAttribute("villesA", volRepository.getVilleOuAeroport("villeA"));
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/ListVolsAvecDate.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String villeD = request.getParameter("villeD");
		String villeA = request.getParameter("villeA");
		String dateD = request.getParameter("dateD");
			
		boolean volR = Boolean.parseBoolean(request.getParameter("volR"));
		
		String link = "ListVolsAvecDate";
		request.setAttribute("link",link);
		if(dateD.equals(null))
		{
			Date date = new Date();
			dateD = date.getDate()+"/"+date.getMonth()+"/"+date.getYear();
		}
		List <VolAvion> volsA = volRepository.ListVolsAvecDate(villeD,villeA,dateD);
		
		if(volsA.isEmpty()) {
			request.setAttribute("messageA","Aucun vol trouvé !");
			}
		else {
			request.setAttribute("volsA",volsA);
			}
	
		
		if(volR) 
		{
			if(!volsA.isEmpty()) 
			{
				List <VolAvion> volsR = new ArrayList<VolAvion>();
				for (VolAvion vol : volsA) {
					
					List <VolAvion>  vols = volRepository.ListVolsAvecDate(villeA,villeD,vol.getDateA());
					
					if(!vols.isEmpty()) {
					volsR.addAll(vols);
						
					}
				}
					
				if(volsR.isEmpty()) {
					request.setAttribute("messageR","Aucun vol retour  trouvé !");
				}
				else {
					request.setAttribute("volsR",volsR);
				}
		  }
		}
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/ResultatRecherche.jsp").forward(request, response);

		
		}

}
