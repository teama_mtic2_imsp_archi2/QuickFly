package imsp.teama.services;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.entities.Avion;
import imsp.teama.repository.AvionRepository;

/**
 * Servlet implementation class AjouterAvionService
 */
@WebServlet("/AjouterAvionService")
public class AjouterAvionService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AvionRepository avionRepository;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjouterAvionService() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init()  throws ServletException{
 		DaoFactory daoFactory = DaoFactory.getInstance();
 		this.avionRepository = daoFactory.getAvionRepository();
 	}

protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/AjouterAvion.jsp").forward(request, response);
	}

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String compagnie = request.getParameter("compagnie");
		String categorie = request.getParameter("categorie");
		String typeAvion = request.getParameter("typeAvion");
		int place = Integer.parseInt(request.getParameter("place"));
		double poidsMax = Double.parseDouble(request.getParameter("poidsMax"));
		boolean surplus = Boolean.parseBoolean(request.getParameter("surplus"));
		Avion avion = new Avion(nom,compagnie,categorie,typeAvion,place,poidsMax,surplus);
		
		if(avionRepository.AjouterAvion(avion))
		{
			String succes="Avion ajouté";
			String link = "AjouterAvion";
			String message = "Ajouter un autre avion ?";
			request.setAttribute("succes",succes);
			request.setAttribute("link",link);
			request.setAttribute("message",message);
			this.getServletContext().getRequestDispatcher("/WEB-INF/ResultatAjout.jsp").forward(request, response);
		}
		else {
			String error = "Une erreur est souvenue, veillez réessayer !";
			String link = "AjouterAvion";
			String message = "Ajouter un autre avion ?";
			request.setAttribute("link",link);
			request.setAttribute("error",error);
			request.setAttribute("message",message);
			this.getServletContext().getRequestDispatcher("/WEB-INF/ResultatAjout.jsp").forward(request, response);
		}
		
	}
}
