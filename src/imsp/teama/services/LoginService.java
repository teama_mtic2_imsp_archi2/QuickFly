package imsp.teama.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import imsp.teama.dao.DaoFactory;
import imsp.teama.entities.User;
import imsp.teama.repository.UserRepository;


@WebServlet("/LoginService")
public class LoginService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private UserRepository userRepository;
	
	public void init()  throws ServletException{
		DaoFactory daoFactory = DaoFactory.getInstance();
		this.userRepository = daoFactory.getUserRepository();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		this.getServletContext().getRequestDispatcher("/WEB-INF/Login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mail = request.getParameter("mail");
		String mdp = request.getParameter("mdp");
		
		if(mail.equalsIgnoreCase("")) {
			String error = "";
			error = "Veillez remplir tous les champs ! ";
			request.setAttribute("error", error);
			this.getServletContext().getRequestDispatcher("/WEB-INF/Login.jsp").forward(request, response);
		}
		
		if(mdp.equalsIgnoreCase("")) {
			String error = "";
			error = "Veillez remplir tous les champs ! ";
			request.setAttribute("error", error);
			this.getServletContext().getRequestDispatcher("/WEB-INF/Login.jsp").forward(request, response);
		}
	
			if(userRepository.checkUser(mail,mdp))
			{
				HttpSession httpSession = request.getSession(true);
				User user = userRepository.getUser( mail);
				httpSession.setAttribute("id", user.getId());
				httpSession.setAttribute("typeUer", user.getTypeUser());
				httpSession.setAttribute("mail",mail);
				String typeUser = user.getTypeUser();
				if(typeUser.equalsIgnoreCase("client")) {
					response.sendRedirect("Index");
				}
				if(typeUser.equalsIgnoreCase("admin")) {
					response.sendRedirect("IndexAdmin");
				}
				
			}
			else {
				String error = "";
				error = "Mail ou mot de passe incorrect !";
				request.setAttribute("error", error);
				this.getServletContext().getRequestDispatcher("/WEB-INF/Login.jsp").forward(request, response);
				}

	}

}
