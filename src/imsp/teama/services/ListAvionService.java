package imsp.teama.services;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import imsp.teama.dao.DaoFactory;
import imsp.teama.repository.AvionRepository;

/**
 * Servlet implementation class ListAvionService
 */
@WebServlet("/ListAvionService")
public class ListAvionService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AvionRepository avionRepository;
       
    public ListAvionService() {
        super();
    }
    
    public void init()  throws ServletException{
 		DaoFactory daoFactory = DaoFactory.getInstance();
 		this.avionRepository = daoFactory.getAvionRepository();
 	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("avions",avionRepository.ListAvion());
		this.getServletContext().getRequestDispatcher("/WEB-INF/ListAvion.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//request.setAttribute("vols",avionRepository.listeAvion());
		//this.getServletContext().getRequestDispatcher("/WEB-INF/ListAvion.jsp").forward(request, response);
	}

}
