package imsp.teama.entities;

public class Bagage {

	private int id;
	private String typeB;
	private double poidsB;
	private int idUser;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTypeB() {
		return typeB;
	}
	public void setTypeB(String typeB) {
		this.typeB = typeB;
	}
	public double getPoidsB() {
		return poidsB;
	}
	public void setPoidsB(double poidsB) {
		this.poidsB = poidsB;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public Bagage(String typeB, double poidsB, int idUser) {
		super();
		this.typeB = typeB;
		this.poidsB = poidsB;
		this.idUser = idUser;
	}
	public Bagage() {
		super();
		// TODO Auto-generated constructor stub
	}
	
		
	
	
}
