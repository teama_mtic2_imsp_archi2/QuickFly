package imsp.teama.entities;

public abstract class User {
	
private int id;
private String typeUser;
private String mail;
private String mdp;

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTypeUser() {
	return typeUser;
}
public void setTypeUser(String typeUser) {
	this.typeUser = typeUser;
}
public String getMail() {
	return mail;
}
public void setMail(String mail) {
	this.mail = mail;
}
public String getMdp() {
	return mdp;
}
public void setMdp(String mdp) {
	this.mdp = mdp;
}
public User() {
	super();
	// TODO Auto-generated constructor stub
}
public User(String typeUser, String mail, String mdp) {
	super();
	this.typeUser = typeUser;
	this.mail = mail;
	this.mdp = mdp;
}






}
