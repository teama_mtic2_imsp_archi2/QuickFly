package imsp.teama.entities;

public class Avion {
	private int id;
	private String nom;
	private String compagnie;
	private String typeAvion;
	private String categorie;
	private int place;
	private double poidsMax;
	private boolean surplus;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCompagnie() {
		return compagnie;
	}
	public void setCompagnie(String compagnie) {
		this.compagnie = compagnie;
	}
	public String getTypeAvion() {
		return typeAvion;
	}
	public void setTypeAvion(String typeAvion) {
		this.typeAvion = typeAvion;
	}
	
public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
public int getPlace() {
		return place;
	}
	public void setPlace(int place) {
		this.place = place;
	}
	public double getPoidsMax() {
		return poidsMax;
	}
	public void setPoidsMax(double poidsMax) {
		this.poidsMax = poidsMax;
	}
	public boolean isSurplus() {
		return surplus;
	}
	public void setSurplus(boolean surplus) {
		this.surplus = surplus;
	}
	
	public Avion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Avion(String nom, String compagnie, String typeAvion, String categorie, int place, double poidsMax,
			boolean surplus) {
		super();
		this.nom = nom;
		this.compagnie = compagnie;
		this.typeAvion = typeAvion;
		this.categorie = categorie;
		this.place = place;
		this.poidsMax = poidsMax;
		this.surplus = surplus;
	}
	
	
}
