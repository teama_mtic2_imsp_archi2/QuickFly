package imsp.teama.entities;

public class Reservation {
	
	private int id;
	private int idUser;
	private int idVol;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdVol() {
		return idVol;
	}
	public void setIdVol(int idVol) {
		this.idVol = idVol;
	}
	public Reservation(int idUser, int idVol) {
		super();
		this.idUser = idUser;
		this.idVol = idVol;
	}
	public Reservation() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	

}
