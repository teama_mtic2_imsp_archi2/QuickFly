package imsp.teama.entities;

public class VolAvion {

	private int id;
	private String villeD;
	private String villeA;
	private String dateD;
	private String dateA;
	private String aeroportD;
	private String aeroportA;
	private Double prix;
	private int tempsEscale;
	private int idAvion;
	private String nom;
	private String compagnie;
	private String typeAvion;
	private String categorie;
	private int place;
	private double poidsMax;
	private boolean surplus;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVilleD() {
		return villeD;
	}
	public void setVilleD(String villeD) {
		this.villeD = villeD;
	}
	public String getVilleA() {
		return villeA;
	}
	public void setVilleA(String villeA) {
		this.villeA = villeA;
	}
	public String getDateD() {
		return dateD;
	}
	public void setDateD(String dateD) {
		this.dateD = dateD;
	}
	public String getDateA() {
		return dateA;
	}
	public void setDateA(String dateA) {
		this.dateA = dateA;
	}
	public String getAeroportD() {
		return aeroportD;
	}
	public void setAeroportD(String aeroportD) {
		this.aeroportD = aeroportD;
	}
	public String getAeroportA() {
		return aeroportA;
	}
	public void setAeroportA(String aeroportA) {
		this.aeroportA = aeroportA;
	}
	public Double getPrix() {
		return prix;
	}
	public void setPrix(Double prix) {
		this.prix = prix;
	}
	public int getTempsEscale() {
		return tempsEscale;
	}
	public void setTempsEscale(int tempsEscale) {
		this.tempsEscale = tempsEscale;
	}
	public int getIdAvion() {
		return idAvion;
	}
	public void setIdAvion(int idAvion) {
		this.idAvion = idAvion;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCompagnie() {
		return compagnie;
	}
	public void setCompagnie(String compagnie) {
		this.compagnie = compagnie;
	}
	public String getTypeAvion() {
		return typeAvion;
	}
	public void setTypeAvion(String typeAvion) {
		this.typeAvion = typeAvion;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public int getPlace() {
		return place;
	}
	public void setPlace(int place) {
		this.place = place;
	}
	public double getPoidsMax() {
		return poidsMax;
	}
	public void setPoidsMax(double poidsMax) {
		this.poidsMax = poidsMax;
	}
	public boolean isSurplus() {
		return surplus;
	}
	public void setSurplus(boolean surplus) {
		this.surplus = surplus;
	}
	public VolAvion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public VolAvion(int id, String villeD, String villeA, String dateD, String dateA, String aeroportD,
			String aeroportA, Double prix, int tempsEscale, int idAvion, String nom, String compagnie,
			String typeAvion, String categorie, int place, double poidsMax, boolean surplus) {
		super();
		this.id = id;
		this.villeD = villeD;
		this.villeA = villeA;
		this.dateD = dateD;
		this.dateA = dateA;
		this.aeroportD = aeroportD;
		this.aeroportA = aeroportA;
		this.prix = prix;
		this.tempsEscale = tempsEscale;
		this.idAvion = idAvion;
		this.nom = nom;
		this.compagnie = compagnie;
		this.typeAvion = typeAvion;
		this.categorie = categorie;
		this.place = place;
		this.poidsMax = poidsMax;
		this.surplus = surplus;
	}
	

}
