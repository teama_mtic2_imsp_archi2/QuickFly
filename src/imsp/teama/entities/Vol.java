package imsp.teama.entities;

public class Vol {
	private int id;
	private String villeD;
	private String villeA;
	private String dateD;
	private String dateA;
	private String aeroportD;
	private String aeroportA;
	private Double prix;
	private int tempsEscale;
	private int idAvion;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVilleD() {
		return villeD;
	}
	public void setVilleD(String villeD) {
		this.villeD = villeD;
	}
	public String getVilleA() {
		return villeA;
	}
	public void setVilleA(String villeA) {
		this.villeA = villeA;
	}
	public String getDateD() {
		return dateD;
	}
	public void setDateD(String dateD) {
		this.dateD = dateD;
	}
	public String getDateA() {
		return dateA;
	}
	public void setDateA(String dateA) {
		this.dateA = dateA;
	}
	public String getAeroportD() {
		return aeroportD;
	}
	public void setAeroportD(String aeroportD) {
		this.aeroportD = aeroportD;
	}
	public String getAeroportA() {
		return aeroportA;
	}
	public void setAeroportA(String aeroportA) {
		this.aeroportA = aeroportA;
	}
	public Double getPrix() {
		return prix;
	}
	public void setPrix(Double prix) {
		this.prix = prix;
	}
	public int getTempsEscale() {
		return tempsEscale;
	}
	public void setTempsEscale(int tempsEscale) {
		this.tempsEscale = tempsEscale;
	}
	public int getIdAvion() {
		return idAvion;
	}
	public void setIdAvion(int idAvion) {
		this.idAvion = idAvion;
	}
	public Vol(String villeD, String villeA, String dateD, String dateA, String aeroportD, String aeroportA, Double prix,
			int tempsEscale, int idAvion) {
		super();
		this.villeD = villeD;
		this.villeA = villeA;
		this.dateD = dateD;
		this.dateA = dateA;
		this.aeroportD = aeroportD;
		this.aeroportA = aeroportA;
		this.prix = prix;
		this.tempsEscale = tempsEscale;
		this.idAvion = idAvion;
	}
	public Vol() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
