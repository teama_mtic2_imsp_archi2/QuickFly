package imsp.teama.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.ForEach;

import imsp.teama.entities.Vol;
import imsp.teama.entities.VolAvion;
import imsp.teama.repository.VolRepository;
import imsp.teama.services.ListVolSelonTypeBagageService;

public class VolDao implements VolRepository{
	private DaoFactory daoFactory;

	public VolDao(DaoFactory daoFactory) {
		super();
		this.daoFactory = daoFactory;
	}

	@Override
	public List<VolAvion> ListVolsAvecDate(String villeD, String villeA, String dateDepart) {
		List<VolAvion> volAvions = new ArrayList<VolAvion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
					+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
					+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
					+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
					+ " where `vol`.`villeD`=? and `vol`.`villeA`=? and `vol`.`dateD`>=? and "
					+ "`vol`.`idAvion`=`avion`.`id`;");
			preparedStatement.setString(1, villeD);
			preparedStatement.setString(2, villeA);
			preparedStatement.setString(3, dateDepart);
			resultSet = preparedStatement.executeQuery();
		
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String villeDepart = resultSet.getString("villeD");
				String villeArrivee = resultSet.getString("villeA");
				String dateD = resultSet.getString("dateD");
				String dateA = resultSet.getString("dateA");
				String aeroportD = resultSet.getString("aeroportD");
				String aeroportA = resultSet.getString("aeroportA");
				double prix = resultSet.getDouble("prix");
				int tempsEscale = resultSet.getInt("tempsEscale");
				int idAvion = resultSet.getInt("idAvion");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				double poidsMax = resultSet.getDouble("poidsMax");
				
				VolAvion volAvion =  new VolAvion();
				volAvion.setId(id);
				volAvion.setVilleD(villeDepart);
				volAvion.setVilleA(villeArrivee);
				volAvion.setDateD(dateD);
				volAvion.setDateA(dateA);
				volAvion.setAeroportD(aeroportD);
				volAvion.setAeroportA(aeroportA);
				volAvion.setPrix(prix);
				volAvion.setTempsEscale(tempsEscale);
				volAvion.setIdAvion(idAvion);
				volAvion.setNom(nom);
				volAvion.setCompagnie(compagnie);
				volAvion.setTypeAvion(typeAvion);
				volAvion.setPlace(getPlaceDisponible(id,idAvion));
				volAvion.setCategorie(categorie);
				volAvion.setPoidsMax(poidsMax);
				volAvions.add(volAvion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return volAvions;
	}

	@Override
	public List<VolAvion> ListVolsAvecPoids(String villeD, String villeA,String dateDepart, double poidsB) {
		List<VolAvion> volAvions = new ArrayList<VolAvion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
					+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
					+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
					+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
					+ " where `vol`.`villeD`=? and `vol`.`villeA`=? and `vol`.`dateD`>=? and "
					+ "`avion`.`poidsMax`>=? and `vol`.`idAvion`=`avion`.`id`;");
			preparedStatement.setString(1, villeD);
			preparedStatement.setString(2, villeA);
			preparedStatement.setString(3, dateDepart);
			preparedStatement.setDouble(4, poidsB);
			resultSet = preparedStatement.executeQuery();
		
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String villeDepart = resultSet.getString("villeD");
				String villeArrivee = resultSet.getString("villeA");
				String dateD = resultSet.getString("dateD");
				String dateA = resultSet.getString("dateA");
				String aeroportD = resultSet.getString("aeroportD");
				String aeroportA = resultSet.getString("aeroportA");
				double prix = resultSet.getDouble("prix");
				int tempsEscale = resultSet.getInt("tempsEscale");
				int idAvion = resultSet.getInt("idAvion");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				double poidsMax = resultSet.getDouble("poidsMax");
				
				VolAvion volAvion =  new VolAvion();
				volAvion.setId(id);
				volAvion.setVilleD(villeDepart);
				volAvion.setVilleA(villeArrivee);
				volAvion.setDateD(dateD);
				volAvion.setDateA(dateA);
				volAvion.setAeroportD(aeroportD);
				volAvion.setAeroportA(aeroportA);
				volAvion.setPrix(prix);
				volAvion.setTempsEscale(tempsEscale);
				volAvion.setIdAvion(idAvion);
				volAvion.setNom(nom);
				volAvion.setCompagnie(compagnie);
				volAvion.setTypeAvion(typeAvion);
				volAvion.setPlace(getPlaceDisponible(id,idAvion));
				volAvion.setCategorie(categorie);
				volAvion.setPoidsMax(poidsMax);
				volAvions.add(volAvion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return volAvions;
	}

	@Override
	public List<VolAvion> ListVolSelonTypeBagage(String villeD, String villeA,String dateDepart,double poidsB, String typeB) {
		List<VolAvion> volAvions = new ArrayList<VolAvion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,`vol`.`dateD`,"
					+ "`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,`vol`.`tempsEscale`,`vol`.`idAvion`,"
					+ "`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,`avion`.`categorie`,`avion`.`poidsMax`"
					+ " from `vol`,`avion` where `vol`.`villeD`=? and `vol`.`villeA`=? and `vol`.`dateD`>=? and `avion`.`poidsMax`>=? "
					+ " and `avion`.`typeAvion`=? and `vol`.`idAvion`=`avion`.`id`;");
			preparedStatement.setString(1, villeD);
			preparedStatement.setString(2, villeA);
			preparedStatement.setString(3, dateDepart);
			preparedStatement.setDouble(4, poidsB);
			preparedStatement.setString(5, typeB);
			resultSet = preparedStatement.executeQuery();
		
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String villeDepart = resultSet.getString("villeD");
				String villeArrivee = resultSet.getString("villeA");
				String dateD = resultSet.getString("dateD");
				String dateA = resultSet.getString("dateA");
				String aeroportD = resultSet.getString("aeroportD");
				String aeroportA = resultSet.getString("aeroportA");
				double prix = resultSet.getDouble("prix");
				int tempsEscale = resultSet.getInt("tempsEscale");
				int idAvion = resultSet.getInt("idAvion");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				double poidsMax = resultSet.getDouble("poidsMax");
				
				VolAvion volAvion =  new VolAvion();
				volAvion.setId(id);
				volAvion.setVilleD(villeDepart);
				volAvion.setVilleA(villeArrivee);
				volAvion.setDateD(dateD);
				volAvion.setDateA(dateA);
				volAvion.setAeroportD(aeroportD);
				volAvion.setAeroportA(aeroportA);
				volAvion.setPrix(prix);
				volAvion.setTempsEscale(tempsEscale);
				volAvion.setIdAvion(idAvion);
				volAvion.setNom(nom);
				volAvion.setCompagnie(compagnie);
				volAvion.setTypeAvion(typeAvion);
				volAvion.setPlace(getPlaceDisponible(id,idAvion));
				volAvion.setCategorie(categorie);
				volAvion.setPoidsMax(poidsMax);
				volAvions.add(volAvion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return volAvions;
	}

	@Override
	public List<VolAvion> ListVolsAvecAeroport(String aeroportDe, String aeroportAr, String dateDepart) {
		List<VolAvion> volAvions = new ArrayList<VolAvion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
					+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
					+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
					+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
					+ " where `vol`.`aeroportD`=? and `vol`.`aeroportA`=? and `vol`.`dateD`>=? and "
					+ "`vol`.`idAvion`=`avion`.`id`;");
			preparedStatement.setString(1, aeroportDe);
			preparedStatement.setString(2, aeroportAr);
			preparedStatement.setString(3, dateDepart);
			resultSet = preparedStatement.executeQuery();
		
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String villeDepart = resultSet.getString("villeD");
				String villeArrivee = resultSet.getString("villeA");
				String dateD = resultSet.getString("dateD");
				String dateA = resultSet.getString("dateA");
				String aeroportD = resultSet.getString("aeroportD");
				String aeroportA = resultSet.getString("aeroportA");
				double prix = resultSet.getDouble("prix");
				int tempsEscale = resultSet.getInt("tempsEscale");
				int idAvion = resultSet.getInt("idAvion");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				double poidsMax = resultSet.getDouble("poidsMax");
				
				VolAvion volAvion =  new VolAvion();
				volAvion.setId(id);
				volAvion.setVilleD(villeDepart);
				volAvion.setVilleA(villeArrivee);
				volAvion.setDateD(dateD);
				volAvion.setDateA(dateA);
				volAvion.setAeroportD(aeroportD);
				volAvion.setAeroportA(aeroportA);
				volAvion.setPrix(prix);
				volAvion.setTempsEscale(tempsEscale);
				volAvion.setIdAvion(idAvion);
				volAvion.setNom(nom);
				volAvion.setCompagnie(compagnie);
				volAvion.setTypeAvion(typeAvion);
				volAvion.setPlace(getPlaceDisponible(id,idAvion));
				volAvion.setCategorie(categorie);
				volAvion.setPoidsMax(poidsMax);
				volAvions.add(volAvion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return volAvions;
	}

	@Override
	public List<String> getVilleOuAeroport(String target) {
		List<String> targets = new ArrayList<String>();
		Connection connexion = null;
		Statement statement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			resultSet = statement.executeQuery("SELECT distinct "+ target +"  FROM vol ;");
			while(resultSet.next())
			{
				String targ = resultSet.getString(target);
				targets.add(targ);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(statement!=null)
					statement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return targets;
	}
	

	@Override
	public boolean AjouterVol(Vol vol) {
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		boolean resp=false;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("INSERT INTO vol(villeD,villeA,dateD,dateA,aeroportD,aeroportA,prix,tempsEscale,idAvion) "
					+ "values(?,?,?,?,?,?,?,?,?);");
			preparedStatement.setString(1, vol.getVilleD());
			preparedStatement.setString(2, vol.getVilleA());
			preparedStatement.setString(3, vol.getDateD());
			preparedStatement.setString(4, vol.getDateA());
			preparedStatement.setString(5, vol.getAeroportD());
			preparedStatement.setString(6, vol.getAeroportA());
			preparedStatement.setDouble(7, vol.getPrix());
			preparedStatement.setInt(8, vol.getTempsEscale());
			preparedStatement.setInt(9, vol.getIdAvion());
			preparedStatement.executeUpdate();
			connexion.commit();
			resp = true ;
		}catch(SQLException e){
			e.printStackTrace();
			try {
				if(connexion!=null)
					connexion.rollback();
			}catch(SQLException e1) {}
		}finally {
			try {
			if(connexion!=null)
				connexion.close();
		}catch(SQLException e2) {}
		}
		
		return resp;
	}

	@Override
	public List<Vol> ListVol() {
		List<Vol> vols = new ArrayList<Vol>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("SELECT * FROM vol ;");
			resultSet = preparedStatement.executeQuery();
		
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String villeDepart = resultSet.getString("villeD");
				String villeArrivee = resultSet.getString("villeA");
				String dateD = resultSet.getString("dateD");
				String dateA = resultSet.getString("dateA");
				String aeroportD = resultSet.getString("aeroportD");
				String aeroportA = resultSet.getString("aeroportA");
				double prix = resultSet.getDouble("prix");
				int tempsEscale = resultSet.getInt("tempsEscale");
				int idAvion = resultSet.getInt("idAvion");
				
				Vol vol =  new Vol();
				vol.setId(id);
				vol.setVilleD(villeDepart);
				vol.setVilleA(villeArrivee);
				vol.setDateD(dateD);
				vol.setDateA(dateA);
				vol.setAeroportD(aeroportD);
				vol.setAeroportA(aeroportA);
				vol.setPrix(prix);
				vol.setTempsEscale(tempsEscale);
				vol.setIdAvion(idAvion);
				vols.add(vol);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return vols;
	}

	
	public int getNombreReservation(int idVol) 
	{
		Connection connexion = null;
		Statement statement= null;
		ResultSet resultSet = null;
		int nb = 0;
		
		try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			resultSet = statement.executeQuery("SELECT count(id) as nb FROM reservation where idVol="+idVol+" ;");
			while(resultSet.next())
			{
				nb = resultSet.getInt("nb");
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(statement!=null)
					statement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		return nb;
	}
	
	public int getPlaceAvion(int idAvion) 
	{
		Connection connexion = null;
		Statement statement= null;
		ResultSet resultSet = null;
		int nb = 0;
		
		try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			resultSet = statement.executeQuery("SELECT place FROM avion WHERE id="+idAvion+" ;");
			while(resultSet.next())
			{
				nb = resultSet.getInt("place");
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(statement!=null)
					statement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		return nb;
	}
	
	@Override
	public int getPlaceDisponible(int idVol, int idAvion) {
		int placeReserve = getNombreReservation(idVol);
		int placeAvion = getPlaceAvion(idAvion);
		return placeAvion-placeReserve;
	}

	
	public List<VolAvion> getVolsAvecDate(String villeD,String date,boolean greaterThanDate)
	{
		List<VolAvion> volAvions = new ArrayList<VolAvion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
	
		try {
			connexion = daoFactory.getConnection();
			if(greaterThanDate)
			{
				preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
						+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
						+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
						+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
						+ " where `vol`.`villeD`=? and `vol`.`dateD`>=? and `vol`.`idAvion`=`avion`.`id`;");
			}
			else {
				preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
						+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
						+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
						+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
						+ " where `vol`.`villeD`=? and `vol`.`dateD`=? and `vol`.`idAvion`=`avion`.`id`;");
			}
			preparedStatement.setString(1, villeD);
			preparedStatement.setString(2, date);
			resultSet = preparedStatement.executeQuery();
	
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String villeDepart = resultSet.getString("villeD");
				String villeArrivee = resultSet.getString("villeA");
				String dateD = resultSet.getString("dateD");
				String dateA = resultSet.getString("dateA");
				String aeroportD = resultSet.getString("aeroportD");
				String aeroportA = resultSet.getString("aeroportA");
				double prix = resultSet.getDouble("prix");
				int tempsEscale = resultSet.getInt("tempsEscale");
				int idAvion = resultSet.getInt("idAvion");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				double poidsMax = resultSet.getDouble("poidsMax");
				
				VolAvion volAvion =  new VolAvion();
				volAvion.setId(id);
				volAvion.setVilleD(villeDepart);
				volAvion.setVilleA(villeArrivee);
				volAvion.setDateD(dateD);
				volAvion.setDateA(dateA);
				volAvion.setAeroportD(aeroportD);
				volAvion.setAeroportA(aeroportA);
				volAvion.setPrix(prix);
				volAvion.setTempsEscale(tempsEscale);
				volAvion.setIdAvion(idAvion);
				volAvion.setNom(nom);
				volAvion.setCompagnie(compagnie);
				volAvion.setTypeAvion(typeAvion);
				volAvion.setPlace(getPlaceDisponible(id,idAvion));
				volAvion.setCategorie(categorie);
				volAvion.setPoidsMax(poidsMax);
				volAvions.add(volAvion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return volAvions;
	}
	
	
	@Override
	public List<VolAvion> getVolsEscaleAvecDate(String villeD, String villeA, String dateD) {
		List<VolAvion> vols = getVolsAvecDate(villeD,dateD,true);
		List<VolAvion> volsF = new ArrayList<VolAvion>();
		List<VolAvion> volsT1 = new ArrayList<VolAvion>();
		List<VolAvion> volsT2 = new ArrayList<VolAvion>();
	
		if(!vols.isEmpty())
		{
			for(Iterator<VolAvion> volIterator = vols.iterator();volIterator.hasNext();)
			{
				VolAvion vol = volIterator.next();
				if(vol.getVilleA().equalsIgnoreCase(villeA))
				{
					volIterator.remove();
				}
			}
			
			if(!vols.isEmpty())
			{
				for(VolAvion vol : vols)
				{
					volsT1 = getVolsAvecDate(vol.getVilleA(),vol.getDateA(),false);
					
					if(!volsT1.isEmpty())
						{	
								for(VolAvion volT1 : volsT1)
								{
									if(volT1.getVilleA().equalsIgnoreCase(villeA))
									{
										if(vol.getTempsEscale()<=720)
										{
											List<VolAvion>  volsDirect= ListVolsAvecDate(villeD,villeA,dateD);
											double prix = 50000000;
											if(volsDirect.isEmpty())
											{	
												vol.setPrix(vol.getPrix()+volT1.getPrix());
												volT1.setPrix(null);
												volsF.add(vol);
												volsF.add(volT1);	
											}
											else {
												for(VolAvion volavion:volsDirect) {
													if(volavion.getPrix()<prix)
														prix = volavion.getPrix();
												}
												
												vol.setPrix(prix*0.8);
												volT1.setPrix(null);
												volsF.add(vol);
												volsF.add(volT1);	
											}
										}
									}
									else
									{	
										volsT2 = getVolsAvecDate(volT1.getVilleA(),volT1.getDateA(),false);
										if(!volsT2.isEmpty())
										{
											for (VolAvion volT2 : volsT2)
												{
													if(volT2.getVilleA().equalsIgnoreCase(villeA))
													{	
														if((vol.getTempsEscale()+volT1.getTempsEscale())<=720)
														{
															
															List<VolAvion>  volsDirect= ListVolsAvecDate(villeD,villeA,dateD);
															double prix = 50000000;
															if(volsDirect.isEmpty())
															{	
																vol.setPrix(vol.getPrix()+volT1.getPrix()+volT2.getPrix());
																volT1.setPrix(null);
																volT2.setPrix(null);
																volsF.add(vol);
																volsF.add(volT1);
																volsF.add(volT2);
															}
															else {
																for(VolAvion volavion:volsDirect) {
																	if(volavion.getPrix()<prix)
																		prix = volavion.getPrix();
																}
																
																vol.setPrix(prix*0.8);
																volT1.setPrix(null);
																volT2.setPrix(null);
																volsF.add(vol);
																volsF.add(volT1);
																volsF.add(volT2);	
															}
														}
													}
												}
										}
									}
								}
						}
				}
			}
		
		
		}
	
		return volsF;
	}
	
		
	public List<VolAvion> getVolsAvecPoids(String villeD,String date,double poids,boolean greaterThanDate)
	{
		List<VolAvion> volAvions = new ArrayList<VolAvion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
	
		try {
			connexion = daoFactory.getConnection();
			if(greaterThanDate)
			{
				preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
						+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
						+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
						+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
						+ " where `vol`.`villeD`=? and `vol`.`dateD`>=? and `avion`.`poidsMax`>=? and `vol`.`idAvion`=`avion`.`id`;");
			}
			else {
				preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
						+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
						+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
						+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
						+ " where `vol`.`villeD`=? and `vol`.`dateD`=? and `avion`.`poidsMax`>=? and `vol`.`idAvion`=`avion`.`id`;");
			}
			preparedStatement.setString(1, villeD);
			preparedStatement.setString(2, date);
			preparedStatement.setDouble(3, poids);
			resultSet = preparedStatement.executeQuery();
	
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String villeDepart = resultSet.getString("villeD");
				String villeArrivee = resultSet.getString("villeA");
				String dateD = resultSet.getString("dateD");
				String dateA = resultSet.getString("dateA");
				String aeroportD = resultSet.getString("aeroportD");
				String aeroportA = resultSet.getString("aeroportA");
				double prix = resultSet.getDouble("prix");
				int tempsEscale = resultSet.getInt("tempsEscale");
				int idAvion = resultSet.getInt("idAvion");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				double poidsMax = resultSet.getDouble("poidsMax");
				
				VolAvion volAvion =  new VolAvion();
				volAvion.setId(id);
				volAvion.setVilleD(villeDepart);
				volAvion.setVilleA(villeArrivee);
				volAvion.setDateD(dateD);
				volAvion.setDateA(dateA);
				volAvion.setAeroportD(aeroportD);
				volAvion.setAeroportA(aeroportA);
				volAvion.setPrix(prix);
				volAvion.setTempsEscale(tempsEscale);
				volAvion.setIdAvion(idAvion);
				volAvion.setNom(nom);
				volAvion.setCompagnie(compagnie);
				volAvion.setTypeAvion(typeAvion);
				volAvion.setPlace(getPlaceDisponible(id,idAvion));
				volAvion.setCategorie(categorie);
				volAvion.setPoidsMax(poidsMax);
				volAvions.add(volAvion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return volAvions;
	}
		
	@Override
	public List<VolAvion> getVolsEscaleAvecPoids(String villeD, String villeA, String dateD,double poids) {
		List<VolAvion> vols = getVolsAvecPoids(villeD,dateD,poids,true);
		List<VolAvion> volsF = new ArrayList<VolAvion>();
		List<VolAvion> volsT1 = new ArrayList<VolAvion>();
		List<VolAvion> volsT2 = new ArrayList<VolAvion>();
	
		if(!vols.isEmpty())
		{
			for(Iterator<VolAvion> volIterator = vols.iterator();volIterator.hasNext();)
			{
				VolAvion vol = volIterator.next();
				if(vol.getVilleA().equalsIgnoreCase(villeA))
				{
					volIterator.remove();
				}
			}
			
			if(!vols.isEmpty())
			{
				for(VolAvion vol : vols)
				{
					volsT1 = getVolsAvecPoids(vol.getVilleA(),vol.getDateA(),poids,false);
					
					if(!volsT1.isEmpty())
						{	
								for(VolAvion volT1 : volsT1)
								{
									if(volT1.getVilleA().equalsIgnoreCase(villeA))
									{
										if(vol.getTempsEscale()<=720)
										{
											List<VolAvion>  volsDirect= ListVolsAvecPoids(villeD,villeA,dateD,poids);
											double prix = 50000000;
											if(volsDirect.isEmpty())
											{	
												vol.setPrix(vol.getPrix()+volT1.getPrix());
												volT1.setPrix(null);
												volsF.add(vol);
												volsF.add(volT1);	
											}
											else {
												for(VolAvion volavion:volsDirect) {
													if(volavion.getPrix()<prix)
														prix = volavion.getPrix();
												}
												
												vol.setPrix(prix*0.8);
												volT1.setPrix(null);
												volsF.add(vol);
												volsF.add(volT1);	
											}
										}
									}
									else
									{	
										volsT2 = getVolsAvecPoids(volT1.getVilleA(),volT1.getDateA(),poids,false);
										if(!volsT2.isEmpty())
										{
											for (VolAvion volT2 : volsT2)
												{
													if(volT2.getVilleA().equalsIgnoreCase(villeA))
													{	
														if((vol.getTempsEscale()+volT1.getTempsEscale())<=720)
														{
															
															List<VolAvion>  volsDirect= ListVolsAvecPoids(villeD,villeA,dateD,poids);
															double prix = 50000000;
															if(volsDirect.isEmpty())
															{	
																vol.setPrix(vol.getPrix()+volT1.getPrix()+volT2.getPrix());
																volT1.setPrix(null);
																volT2.setPrix(null);
																volsF.add(vol);
																volsF.add(volT1);
																volsF.add(volT2);
															}
															else {
																for(VolAvion volavion:volsDirect) {
																	if(volavion.getPrix()<prix)
																		prix = volavion.getPrix();
																}
																
																vol.setPrix(prix*0.8);
																volT1.setPrix(null);
																volT2.setPrix(null);
																volsF.add(vol);
																volsF.add(volT1);
																volsF.add(volT2);	
															}
														}
													}
												}
										}
									}
								}
						}
				}
			}
		
		
		}
	
		return volsF;
	}

	
	public List<VolAvion> getVolsSelonTypeBagage(String villeD,String date,double poids,String typeBagage,boolean greaterThanDate)
	{
		List<VolAvion> volAvions = new ArrayList<VolAvion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
	
		try {
			connexion = daoFactory.getConnection();
			if(greaterThanDate)
			{
				preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
						+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
						+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
						+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
						+ " where `vol`.`villeD`=? and `vol`.`dateD`>=? and `avion`.`poidsMax`>=? and `avion`.`typeAvion`=? and `vol`.`idAvion`=`avion`.`id`;");
			}
			else {
				preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
						+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
						+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
						+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
						+ " where `vol`.`villeD`=? and `vol`.`dateD`=? and `avion`.`poidsMax`>=? and `avion`.`typeAvion`=? and `vol`.`idAvion`=`avion`.`id`;");
			}
			preparedStatement.setString(1, villeD);
			preparedStatement.setString(2, date);
			preparedStatement.setDouble(3, poids);
			preparedStatement.setString(4, typeBagage);
			resultSet = preparedStatement.executeQuery();
	
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String villeDepart = resultSet.getString("villeD");
				String villeArrivee = resultSet.getString("villeA");
				String dateD = resultSet.getString("dateD");
				String dateA = resultSet.getString("dateA");
				String aeroportD = resultSet.getString("aeroportD");
				String aeroportA = resultSet.getString("aeroportA");
				double prix = resultSet.getDouble("prix");
				int tempsEscale = resultSet.getInt("tempsEscale");
				int idAvion = resultSet.getInt("idAvion");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				double poidsMax = resultSet.getDouble("poidsMax");
				
				VolAvion volAvion =  new VolAvion();
				volAvion.setId(id);
				volAvion.setVilleD(villeDepart);
				volAvion.setVilleA(villeArrivee);
				volAvion.setDateD(dateD);
				volAvion.setDateA(dateA);
				volAvion.setAeroportD(aeroportD);
				volAvion.setAeroportA(aeroportA);
				volAvion.setPrix(prix);
				volAvion.setTempsEscale(tempsEscale);
				volAvion.setIdAvion(idAvion);
				volAvion.setNom(nom);
				volAvion.setCompagnie(compagnie);
				volAvion.setTypeAvion(typeAvion);
				volAvion.setPlace(getPlaceDisponible(id,idAvion));
				volAvion.setCategorie(categorie);
				volAvion.setPoidsMax(poidsMax);
				volAvions.add(volAvion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return volAvions;
	}
		
	@Override
	public List<VolAvion> getVolsEscaleSelonTypeBagage(String villeD, String villeA, String dateD,double poids,String typeBagage) {
		List<VolAvion> vols = getVolsSelonTypeBagage(villeD,dateD,poids,typeBagage,true);
		List<VolAvion> volsF = new ArrayList<VolAvion>();
		List<VolAvion> volsT1 = new ArrayList<VolAvion>();
		List<VolAvion> volsT2 = new ArrayList<VolAvion>();
	
		if(!vols.isEmpty())
		{
			for(Iterator<VolAvion> volIterator = vols.iterator();volIterator.hasNext();)
			{
				VolAvion vol = volIterator.next();
				if(vol.getVilleA().equalsIgnoreCase(villeA))
				{
					volIterator.remove();
				}
			}
			
			if(!vols.isEmpty())
			{
				for(VolAvion vol : vols)
				{
					volsT1 = getVolsSelonTypeBagage(vol.getVilleA(),vol.getDateA(),poids,typeBagage,false);
					
					if(!volsT1.isEmpty())
						{	
								for(VolAvion volT1 : volsT1)
								{
									if(volT1.getVilleA().equalsIgnoreCase(villeA))
									{
										if(vol.getTempsEscale()<=720)
										{
											List<VolAvion>  volsDirect= ListVolSelonTypeBagage(villeD,villeA,dateD,poids,typeBagage);
											double prix = 50000000;
											if(volsDirect.isEmpty())
											{	
												vol.setPrix(vol.getPrix()+volT1.getPrix());
												volT1.setPrix(null);
												volsF.add(vol);
												volsF.add(volT1);	
											}
											else {
												for(VolAvion volavion:volsDirect) {
													if(volavion.getPrix()<prix)
														prix = volavion.getPrix();
												}
												
												vol.setPrix(prix*0.8);
												volT1.setPrix(null);
												volsF.add(vol);
												volsF.add(volT1);	
											}
										}
									}
									else
									{	
										volsT2 = getVolsSelonTypeBagage(volT1.getVilleA(),volT1.getDateA(),poids,typeBagage,false);
										if(!volsT2.isEmpty())
										{
											for (VolAvion volT2 : volsT2)
												{
													if(volT2.getVilleA().equalsIgnoreCase(villeA))
													{	
														if((vol.getTempsEscale()+volT1.getTempsEscale())<=720)
														{
															
															List<VolAvion>  volsDirect= ListVolSelonTypeBagage(villeD,villeA,dateD,poids,typeBagage);
															double prix = 50000000;
															if(volsDirect.isEmpty())
															{	
																vol.setPrix(vol.getPrix()+volT1.getPrix()+volT2.getPrix());
																volT1.setPrix(null);
																volT2.setPrix(null);
																volsF.add(vol);
																volsF.add(volT1);
																volsF.add(volT2);
															}
															else {
																for(VolAvion volavion:volsDirect) {
																	if(volavion.getPrix()<prix)
																		prix = volavion.getPrix();
																}
																
																vol.setPrix(prix*0.8);
																volT1.setPrix(null);
																volT2.setPrix(null);
																volsF.add(vol);
																volsF.add(volT1);
																volsF.add(volT2);	
															}
														}
													}
												}
										}
									}
								}
						}
				}
			}
		
		
		}
	
		return volsF;
	}


	
	public List<VolAvion> getVolsAvecAeroport(String aeroportDepart,String date,boolean greaterThanDate)
	{
		List<VolAvion> volAvions = new ArrayList<VolAvion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
	
		try {
			connexion = daoFactory.getConnection();
			if(greaterThanDate)
			{
				preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
						+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
						+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
						+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
						+ " where `vol`.`aeroportD`=? and `vol`.`dateD`>=? and `vol`.`idAvion`=`avion`.`id`;");
			}
			else {
				preparedStatement = connexion.prepareStatement("select `vol`.`id`,`vol`.`villeD`,`vol`.`villeA`,"
						+ "`vol`.`dateD`,`vol`.`dateA`,`vol`.`aeroportD`,`vol`.`aeroportA`,`vol`.`prix`,"
						+ "`vol`.`tempsEscale`,`vol`.`idAvion`,`avion`.`nom`,`avion`.`compagnie`,`avion`.`typeAvion`,"
						+ "`avion`.`categorie`,`avion`.`poidsMax`  from `vol`,`avion`"
						+ " where `vol`.`aeroportD`=? and `vol`.`dateD`=? and `vol`.`idAvion`=`avion`.`id`;");
			}
			preparedStatement.setString(1, aeroportDepart);
			preparedStatement.setString(2, date);
			resultSet = preparedStatement.executeQuery();
	
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String villeDepart = resultSet.getString("villeD");
				String villeArrivee = resultSet.getString("villeA");
				String dateD = resultSet.getString("dateD");
				String dateA = resultSet.getString("dateA");
				String aeroportD = resultSet.getString("aeroportD");
				String aeroportA = resultSet.getString("aeroportA");
				double prix = resultSet.getDouble("prix");
				int tempsEscale = resultSet.getInt("tempsEscale");
				int idAvion = resultSet.getInt("idAvion");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				double poidsMax = resultSet.getDouble("poidsMax");
				
				VolAvion volAvion =  new VolAvion();
				volAvion.setId(id);
				volAvion.setVilleD(villeDepart);
				volAvion.setVilleA(villeArrivee);
				volAvion.setDateD(dateD);
				volAvion.setDateA(dateA);
				volAvion.setAeroportD(aeroportD);
				volAvion.setAeroportA(aeroportA);
				volAvion.setPrix(prix);
				volAvion.setTempsEscale(tempsEscale);
				volAvion.setIdAvion(idAvion);
				volAvion.setNom(nom);
				volAvion.setCompagnie(compagnie);
				volAvion.setTypeAvion(typeAvion);
				volAvion.setPlace(getPlaceDisponible(id,idAvion));
				volAvion.setCategorie(categorie);
				volAvion.setPoidsMax(poidsMax);
				volAvions.add(volAvion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return volAvions;
	}
		
	@Override
	public List<VolAvion> getVolsEscaleAvecAeroport(String aeroportD, String aeroportA, String dateD) {
		List<VolAvion> vols = getVolsAvecAeroport(aeroportD,dateD,true);
		List<VolAvion> volsF = new ArrayList<VolAvion>();
		List<VolAvion> volsT1 = new ArrayList<VolAvion>();
		List<VolAvion> volsT2 = new ArrayList<VolAvion>();
	
		if(!vols.isEmpty())
		{
			for(Iterator<VolAvion> volIterator = vols.iterator();volIterator.hasNext();)
			{
				VolAvion vol = volIterator.next();
				if(vol.getAeroportA().equalsIgnoreCase(aeroportA))
				{
					volIterator.remove();
				}
			}
			
			if(!vols.isEmpty())
			{
				for(VolAvion vol : vols)
				{
					volsT1 = getVolsAvecAeroport(vol.getAeroportA(),vol.getDateA(),false);
					
					if(!volsT1.isEmpty())
						{	
								for(VolAvion volT1 : volsT1)
								{
									if(volT1.getAeroportA().equalsIgnoreCase(aeroportA))
									{
										if(vol.getTempsEscale()<=720)
										{
											List<VolAvion>  volsDirect= ListVolsAvecAeroport(aeroportD,aeroportA,dateD);
											double prix = 50000000;
											if(volsDirect.isEmpty())
											{	
												vol.setPrix(vol.getPrix()+volT1.getPrix());
												volT1.setPrix(null);
												volsF.add(vol);
												volsF.add(volT1);	
											}
											else {
												for(VolAvion volavion:volsDirect) {
													if(volavion.getPrix()<prix)
														prix = volavion.getPrix();
												}
												
												vol.setPrix(prix*0.8);
												volT1.setPrix(null);
												volsF.add(vol);
												volsF.add(volT1);	
											}
										}
									}
									else
									{	
										volsT2 = getVolsAvecAeroport(volT1.getAeroportA(),volT1.getDateA(),false);
										if(!volsT2.isEmpty())
										{
											for (VolAvion volT2 : volsT2)
												{
													if(volT2.getAeroportA().equalsIgnoreCase(aeroportA))
													{	
														if((vol.getTempsEscale()+volT1.getTempsEscale())<=720)
														{
															
															List<VolAvion>  volsDirect= ListVolsAvecAeroport(aeroportD,aeroportA,dateD);
															double prix = 50000000;
															if(volsDirect.isEmpty())
															{	
																vol.setPrix(vol.getPrix()+volT1.getPrix()+volT2.getPrix());
																volT1.setPrix(null);
																volT2.setPrix(null);
																volsF.add(vol);
																volsF.add(volT1);
																volsF.add(volT2);
															}
															else {
																for(VolAvion volavion:volsDirect) {
																	if(volavion.getPrix()<prix)
																		prix = volavion.getPrix();
																}
																
																vol.setPrix(prix*0.8);
																volT1.setPrix(null);
																volT2.setPrix(null);
																volsF.add(vol);
																volsF.add(volT1);
																volsF.add(volT2);	
															}
														}
													}
												}
										}
									}
								}
						}
				}
			}
		
		
		}
	
		return volsF;
	}


	
}

