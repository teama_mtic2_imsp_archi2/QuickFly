package imsp.teama.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.ForEach;

import imsp.teama.entities.Avion;
import imsp.teama.repository.AvionRepository;

public class AvionDao implements AvionRepository{
	private DaoFactory daoFactory;

	public AvionDao(DaoFactory daoFactory) {
		super();
		this.daoFactory = daoFactory;
	}
	
	@Override
	public List<Avion> ListAvion() {
		List<Avion> avions = new ArrayList<Avion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("SELECT * FROM avion ;");
			resultSet = preparedStatement.executeQuery();
		
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				int place = resultSet.getInt("place");
				double poidsMax = resultSet.getDouble("poidsMax");
				boolean surplus = resultSet.getBoolean("surplus");
				
				Avion avion =  new Avion();
				avion.setId(id);
				avion.setNom(nom);
				avion.setCompagnie(compagnie);
				avion.setTypeAvion(typeAvion);
				avion.setCategorie(categorie);
				avion.setPlace(place);
				avion.setPoidsMax(poidsMax);
				avion.setSurplus(surplus);
				avions.add(avion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return avions;
	}

	@Override
	public List<Avion> ListAvionParCategorie(String categorieA) {
		List<Avion> avions = new ArrayList<Avion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("SELECT * FROM avion WHERE categorie = ?;");
			preparedStatement.setString(1, categorieA);
			resultSet = preparedStatement.executeQuery();
		
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				int place = resultSet.getInt("place");
				double poidsMax = resultSet.getDouble("poidsMax");
				boolean surplus = resultSet.getBoolean("surplus");
				
				Avion avion =  new Avion();
				avion.setId(id);
				avion.setNom(nom);
				avion.setCompagnie(compagnie);
				avion.setTypeAvion(typeAvion);
				avion.setCategorie(categorie);
				avion.setPlace(place);
				avion.setPoidsMax(poidsMax);
				avion.setSurplus(surplus);
				avions.add(avion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return avions;
	}

	@Override
	public List<Avion> ListAvionParType(String TypeAvion) {
		List<Avion> avions = new ArrayList<Avion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("SELECT * FROM avion WHERE typeAvion = ?;");
			preparedStatement.setString(1, TypeAvion);
			resultSet = preparedStatement.executeQuery();
		
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				int place = resultSet.getInt("place");
				double poidsMax = resultSet.getDouble("poidsMax");
				boolean surplus = resultSet.getBoolean("surplus");
				
				Avion avion =  new Avion();
				avion.setId(id);
				avion.setNom(nom);
				avion.setCompagnie(compagnie);
				avion.setTypeAvion(typeAvion);
				avion.setCategorie(categorie);
				avion.setPlace(place);
				avion.setPoidsMax(poidsMax);
				avion.setSurplus(surplus);
				avions.add(avion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return avions;
	}
	
	@Override
	public List<Avion> ListAvionParCategorieOuType(String categorieA, String typeAvionA) {
		List<Avion> avions = new ArrayList<Avion>();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("SELECT * FROM avion WHERE categorie = ? AND typeAvion=? ;");
			preparedStatement.setString(1, categorieA);
			preparedStatement.setString(2, typeAvionA);
			resultSet = preparedStatement.executeQuery();
		
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				int place = resultSet.getInt("place");
				double poidsMax = resultSet.getDouble("poidsMax");
				boolean surplus = resultSet.getBoolean("surplus");
				
				Avion avion =  new Avion();
				avion.setId(id);
				avion.setNom(nom);
				avion.setCompagnie(compagnie);
				avion.setTypeAvion(typeAvion);
				avion.setCategorie(categorie);
				avion.setPlace(place);
				avion.setPoidsMax(poidsMax);
				avion.setSurplus(surplus);
				avions.add(avion);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return avions;
	}
	
	@Override
	public Avion getAvion(int idA) {
		Avion avion =  new Avion();
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("SELECT * FROM avion WHERE id = ?;");
			preparedStatement.setInt(1, idA);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String nom = resultSet.getString("nom");
				String compagnie = resultSet.getString("compagnie");
				String typeAvion = resultSet.getString("typeAvion");
				String categorie = resultSet.getString("categorie");
				int place = resultSet.getInt("place");
				double poidsMax = resultSet.getDouble("poidsMax");
				boolean surplus = resultSet.getBoolean("surplus");
				
				avion.setId(id);
				avion.setNom(nom);
				avion.setCompagnie(compagnie);
				avion.setTypeAvion(typeAvion);
				avion.setCategorie(categorie);
				avion.setPlace(place);
				avion.setPoidsMax(poidsMax);
				avion.setSurplus(surplus);
			}
				
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return avion;
	}

	@Override
	public List<String> ListAttributAvion(String attribut) {
		List<String> attributs = new ArrayList<String>();
		Connection connexion = null;
		Statement statement= null;
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			resultSet = statement.executeQuery("SELECT distinct "+ attribut +"  FROM avion ;");
			while(resultSet.next())
			{
				String targ = resultSet.getString(attribut);
				attributs.add(targ);
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(statement!=null)
					statement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return attributs;
	}

	@Override
	public boolean AjouterAvion(Avion avion) {
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		boolean resp=false;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("INSERT INTO avion(nom,compagnie,categorie,typeAvion,place,poidsMax,surplus) "
					+ "values(?,?,?,?,?,?,?);");
			preparedStatement.setString(1, avion.getNom());
			preparedStatement.setString(2, avion.getCompagnie());
			preparedStatement.setString(3, avion.getCategorie());
			preparedStatement.setString(4, avion.getTypeAvion());
			preparedStatement.setInt(5, avion.getPlace());
			preparedStatement.setDouble(6, avion.getPoidsMax());
			preparedStatement.setBoolean(7, avion.isSurplus());
			preparedStatement.executeUpdate();
			connexion.commit();
			resp = true ;
		}catch(SQLException e){
			e.printStackTrace();
			try {
				if(connexion!=null)
					connexion.rollback();
			}catch(SQLException e1) {}
		}finally {
			try {
			if(connexion!=null)
				connexion.close();
		}catch(SQLException e2) {}
		}
		
		return resp;
	}

	
	@Override
	public boolean ConfigurerAvion(int id, double poidsMax) {
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		boolean resp=false;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("UPDATE avion SET poidsMax=? WHERE id=?");
			
			preparedStatement.setDouble(1,poidsMax);
			preparedStatement.setInt(2,id);
			
			preparedStatement.executeUpdate();
			connexion.commit();
			resp = true ;
		}catch(SQLException e){
			e.printStackTrace();
			try {
				if(connexion!=null)
					connexion.rollback();
			}catch(SQLException e1) {}
		}finally {
			try {
			if(connexion!=null)
				connexion.close();
		}catch(SQLException e2) {}
		}
		
		return resp;
	}

}
