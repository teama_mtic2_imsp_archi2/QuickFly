package imsp.teama.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import imsp.teama.repository.AvionRepository;
import imsp.teama.repository.UserRepository;
import imsp.teama.repository.VolRepository;

public class DaoFactory {
private String url;
private String username;
private String password;

public DaoFactory(String url, String username, String password) {
	super();
	this.url = url;
	this.username = username;
	this.password = password;
}

public static DaoFactory getInstance() {
	try {
		Class.forName("org.mariadb.jdbc.Driver");
	//	Class.forName("org.mysql.jdbc.Driver");
	}catch(ClassNotFoundException e) {};
	
	DaoFactory instance = new DaoFactory("jdbc:mariadb://localhost:3306/QuickFly","root","66091913");
	//DaoFactory instance = new DaoFactory("jdbc:mysql://localhost:3306/QuickFly","root","66091913");
	return instance;
			
	}

public Connection getConnection() throws SQLException{
	Connection connexion = DriverManager.getConnection(url,username,password);
	connexion.setAutoCommit(false);
	return	connexion;
}

//Recupération du dao
public UserRepository getUserRepository() {
	return new UserDao(this);
}

//Recupération du dao
public VolRepository getVolRepository() {
	return new VolDao(this);
}


public AvionRepository getAvionRepository() {
	return new AvionDao(this) ;
}

}