package imsp.teama.dao;

import java.sql.*;

import imsp.teama.entities.Client;
import imsp.teama.entities.User;
import imsp.teama.repository.UserRepository;

public class UserDao implements UserRepository {
private DaoFactory daoFactory;
	
	public UserDao(DaoFactory daoFactory) {
	super();
	this.daoFactory = daoFactory;
	}


	@Override
	public boolean ajouter(User user) {
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		boolean result = false;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("INSERT INTO user(typeUser,mail,mdp) VALUES(?,?,?);");
			preparedStatement.setString(1, user.getTypeUser());
			preparedStatement.setString(2, user.getMail());
			preparedStatement.setString(3, user.getMdp());
			result = true; 
			preparedStatement.executeUpdate();
			connexion.commit();
		}catch(SQLException e){
			e.printStackTrace();
			try {
				if(connexion!=null)
					connexion.rollback();
			}catch(SQLException e1) {}
		}finally {
			try {
			if(connexion!=null)
				connexion.close();
		}catch(SQLException e2) {}
		}
		return result;
	}

	@Override
	public boolean checkMail(String mail) {		
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		
		ResultSet resultSet = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("SELECT mail FROM user ;");
			resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next())
			{
				if(mail.equalsIgnoreCase(resultSet.getString("mail"))){
					return true;
					}
			}	
			}
		catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
	return false;
	}


	@Override
	public User getUser(String mail) {
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		ResultSet resultSet = null;
		User user =  new Client();
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("SELECT * FROM user WHERE mail=? ;");
			preparedStatement.setString(1, mail);
			resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String typeUser = resultSet.getString("typeUser");
				String email = resultSet.getString("mail");
				String mdp = resultSet.getString("mdp");
				
				user.setId(id);
				user.setTypeUser(typeUser);
				user.setMail(email);
				user.setMdp(mdp);
			}	
				
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
		
		return user;
	}

	@Override
	public boolean checkUser(String mail,String mdp) {
		Connection connexion = null;
		PreparedStatement preparedStatement= null;
		
		ResultSet resultSet = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement("SELECT mail,mdp FROM user ;");
			resultSet = preparedStatement.executeQuery();
			
				while(resultSet.next())
				{
					if(mail.equalsIgnoreCase(resultSet.getString("mail")))
					{
						if(mdp.equalsIgnoreCase(resultSet.getString("mdp")))
						{
								return true;
						}
					}
				}	
			}
		catch(SQLException e){
			e.printStackTrace();
		}finally {
			try {
				if(resultSet!=null)
					resultSet.close();
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connexion!=null)
					connexion.close();
			}catch(SQLException ignore) {}
		}
	return false;
	}
	
	

}
