package imsp.teama.repository;


import imsp.teama.entities.User;

public interface UserRepository {
	boolean ajouter(User user);
	boolean checkMail(String mail);
	boolean checkUser(String mail,String mdp);
	public User getUser(String mail);
	

}
