package imsp.teama.repository;


import java.util.List;

import imsp.teama.entities.Vol;
import imsp.teama.entities.VolAvion;


public interface VolRepository {
	public List <VolAvion> ListVolsAvecDate(String villeD,String villeA, String dateDepart) ;
	public List <VolAvion> ListVolsAvecPoids(String villeD,String villeA,String dateDepart,double poidsB);
	public List <VolAvion> ListVolSelonTypeBagage(String villeD,String villeA,String dateDepart,double poidsB,String typeB);
	public List <VolAvion> ListVolsAvecAeroport(String aeroportD,String aeroportA,String dateDepart);
	public List <String> getVilleOuAeroport(String target);
	public boolean AjouterVol(Vol vol);
	public List <Vol> ListVol();
	public int getPlaceDisponible(int idVol,int idAvion);
	public List <VolAvion> getVolsEscaleAvecDate(String villeD,String villeA,String dateD);
	public List<VolAvion> getVolsEscaleAvecPoids(String villeD, String villeA, String dateD,double poids);
	public List<VolAvion> getVolsEscaleSelonTypeBagage(String villeD, String villeA, String dateD,double poids,String typeBagage);
	public List<VolAvion> getVolsEscaleAvecAeroport(String aeroportD, String aeroportA, String dateD) ;
}
