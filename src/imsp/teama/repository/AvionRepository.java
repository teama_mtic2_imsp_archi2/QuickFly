package imsp.teama.repository;

import java.util.List;

import imsp.teama.entities.Avion;

public interface AvionRepository {
	public List <Avion> ListAvion();
	public List <Avion> ListAvionParCategorie(String categorie);
	public List <Avion> ListAvionParType(String typeAvion);
	public List <Avion> ListAvionParCategorieOuType(String categorie,String typeAvion);
	public Avion getAvion(int id);
	public List <String> ListAttributAvion(String attribut);
	public boolean AjouterAvion(Avion avion);
	public boolean ConfigurerAvion(int id, double poidsMax);
}
