 
<!DOCTYPE html>
<html lang="en">
	<head>
		
 <%@include file="css.jsp" %>	

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuClient.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
     
      <div class="overlay"></div>
      <div class="container mainContainer">
        <div class="row align-items-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Explore The World With Benin Airline</h2>
            <p class="lead mb-5 probootstrap-animate">
            To provide unsurpassed, safe and reliable services in air transportation, 
            including strategically linking Benin with the outside world, while ensuring a fair return on investment". 
            "We have a vision to be the airline of obvious choice in the markets we serve<a href="#" target="_blank"> QuickyFly</a> Project <a href="Members" target="_blank">MTIC 2 2018-2019</a></p>
            <p class="probootstrap-animate">
              <a href="#" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Stay update with Benin Airline</a> 
            </p>
          </div> 
          <div class="col-md probootstrap-animate">
          <h2 style="color:white;margin-left:130px;">Recherche un vol </h2>
            <form  class="probootstrap-form" method="post">
            <c:if test="${!empty error}"><p style="color:red; text-align:center;"><c:out value="${error}"/></p></c:if>
              <div class="form-group">
                <div class="row mb-3">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single">Ville de d�part</label>
                      <label for="id_label_single" style="width: 100%;">
                        <select name="villeD" class="js-example-basic-single js-states form-control" id="id_label_single" style="width: 100%;">
                           <c:forEach items="${villesD}" var="villeD" varStatus="status">
		                      <option value="${villeD}">${villeD}</option>
							</c:forEach>
                        </select>
                      </label>

                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single2">Ville d'arriv�e</label>
                      <div class="probootstrap_select-wrap">
                        <label for="id_label_single2" style="width: 100%;">
                        <select name="villeA" class="js-example-basic-single js-states form-control" id="id_label_single2" style="width: 100%;">
                          <c:forEach items="${villesA}" var="villeA" varStatus="status">
		                      <option value="${villeA}">${villeA}</option>
							</c:forEach>
                        </select>
                      </label>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END row -->
                <div class="row mb-5">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Date de d�part</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-calendar"></span> 
                        <input name="dateD" type="text" id="probootstrap-date-departure" class="form-control" placeholder="Choisir une date">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END row -->
                <div class="row">
                  <div class="col-md">
                    <label for="roundway"><input type="checkbox" value="true" id="roundway" name="volE">Vols Escale ?</label>
                  </div>
                  <div class="col-md">
                    <label for="oneway"><input type="checkbox" value="true" id="oneway" name="volR">Vol retour ?</label>
                  </div>
                  <div class="col-md">
                    <input type="submit" value="Submit" class="btn btn-primary btn-block">
                  </div>
                </div>
              </div>
            </form>
          </div> 
         </div>
          </div>
      
    </section>
    
     <%@include file="js.jsp" %>	
	</body>
</html>