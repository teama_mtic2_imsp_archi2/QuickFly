 
<!DOCTYPE html>
<html lang="en">
	<head>
	 
    <%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuAdmin.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover align-items-center  overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
     <c:if test="${empty vols}">
	   <div class="container">
    		<h2 id="no-result-found" class="heading mb-2 display-4 font-light probootstrap-animate"><c:out value="Vous n'avez pas encore ajout� de vols"/></h2>
             <div class="row col-md" id="no-result-link">  <a href="AjouterVol" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Ajouter un vol ?</a>  
        </div>  
       </div>     
     </c:if>
       <c:if test="${!empty vols}">
           <div class="overlay"></div>
      	<div class="container mainContainer">
			<div class="row col-md align-items-center table-responsive probootstrap-animate">
			 <h2 style="color:white; margin-left:400px;">Liste des vols </h2>	
				    <table class="table table-bordered" style="color:white;">
				      <thead>
				        <tr>
				          <th>N�</th>
				          <th>Id Vol</th>
				          <th>Ville D�part</th>
				          <th>Ville Arriv�e</th>
				          <th>Date D�part</th>
				          <th>Date Arriv�e</th>
				          <th>A�roport D�part</th>
				          <th>A�roport Arriv�e</th>
				          <th>Prix</th>
				          <th>Temps Escale</th>
				          <th>Id Avion</th>
				        </tr>
				      </thead>
				      <tbody>
				      <c:forEach items="${vols}" var="vol" varStatus="status">
					  <tr><td><c:out value="${status.count}"/></td><td><c:out value="${vol.id }"/></td><td><c:out value="${vol.villeD }"/></td>
					  <td><c:out value="${vol.villeA }"/></td><td><c:out value="${vol.dateD }"/></td><td><c:out value="${vol.dateA }"/></td>
					  <td><c:out value="${vol.aeroportD }"/></td><td><c:out value="${vol.aeroportA }"/></td>
					  <td><c:out value="${vol.prix }"/></td><td><c:out value="${vol.tempsEscale }"/></td>
					  <td><c:out value="${vol.idAvion}"/></td></tr>
					  </c:forEach>
				      </tbody>
				    </table>
			  </div>
			  </div>
          </c:if>
    </section>
    
     
    <!-- END section -->  
      
   <%@include file="js.jsp" %>
	</body>
</html>