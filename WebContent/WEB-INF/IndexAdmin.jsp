 
<!DOCTYPE html>
<html lang="en">
	<head>
		
    <%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuAdmin.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container mainContainer">
        <div class="row align-items-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Explore The World With Benin Airline</h2>
            <p class="lead mb-5 probootstrap-animate">
            To provide unsurpassed, safe and reliable services in air transportation, 
            including strategically linking Benin with the outside world, while ensuring a fair return on investment". 
            "We have a vision to be the airline of obvious choice in the markets we serve<a href="#" target="_blank"> Quicky Fly</a> Project <a href="#" target="_blank">MTIC 2 2018-2019</a></p>
            <p class="probootstrap-animate" style="text-align:center">
              <a href="#" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Welcome Monsieur Sidoine ODE ADMIN</a> 
            </p>
          </div>
         </div>
          </div>
    </section>
    
     
    <!-- END section -->    
  <%@include file="js.jsp" %>
	</body>
</html>