 
<!DOCTYPE html>
<html lang="en">
	<head>
	 <%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuAdmin.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover align-items-center  overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
     <c:if test="${!empty succes}">
     <div class="overlay"></div>
	   <div class="container" id="succes-result-ajout">
    		<h2 id="" class="heading mb-2 display-4 font-light probootstrap-animate"><c:out value="${succes}"/></h2>
             <div class="row col-md " id="succes-result-link" >  
             <a href="${link}" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3"><c:out value="${message}"/></a>  
        </div> 
        </div>     
     </c:if>
     <c:if test="${!empty error}">
     <div class="overlay"></div>
	   <div class="container">
    		<h2 id="succes-result-ajout" class="heading mb-2 display-4 font-light probootstrap-animate"><c:out value="${error}"/></h2>
             <div class="row col-md " id="succes-result-link" >  <a href="${link}" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3"><c:out value="${message}"/></a>  
        	</div> 
        </div>     
     </c:if>
      
    </section>
    
     
    <!-- END section -->    
    <%@include file="js.jsp" %>
	</body>
</html>