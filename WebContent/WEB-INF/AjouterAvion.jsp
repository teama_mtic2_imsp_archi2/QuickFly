 
<!DOCTYPE html>
<html lang="en">
	<head>
		
<%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuAdmin.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container mainContainer">
        <div class="row align-items-center">
          <div class="col-md probootstrap-animate">
          <div class="row md-8 ajoutElement">
		     <h2>Ajout d'un nouveau avion</h2>
		   </div>
            <form  class="probootstrap-form" method="post">
              <div class="form-group">
                <div class="row mb-3">
                 <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Nom de l'avion</label>
                        <input required="required" type="text" name="nom"  class="form-control" placeholder="Nom de l'avion">
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Compagnie</label>
                        <input required="required" type="text" name="compagnie"  class="form-control" placeholder="compagnie">
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">categorie</label>
                        <input required="required" type="text" name="categorie"  class="form-control" placeholder="categorie">
                    </div>
                  </div>
                </div>
              </div>
                <!-- END row -->
              <div class="form-group">
                <div class="row mb-3">
                 <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Type de l'avion</label>
                        <input required="required" type="text" name="typeAvion"  class="form-control" placeholder="Type de l'avion">
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Nombre de place</label>
                        <input required="required" type="text" name="place"  class="form-control" placeholder="Nombre de place">
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Poids maximal</label>
                        <input required="required" type="text" name="poidsMax"  class="form-control" placeholder="Poids maximal">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row mb-2">
                 <div class="col-md">
                    <label for="round" class="mr-5"><input value="true" type="radio" id="round" name="surplus"> Accepte surplus</label>
                    <label for="oneway"><input value="false" checked  type="radio" id="oneway" name="surplus">Pas de surplus</label>
                  </div>
                  <div class="col-md">
                    <input type="submit" value="Ajouter" class="btn btn-primary btn-block">
                  </div>
                </div>
               </div>
                <!-- END row -->
            </form>
          </div>
          </div>  
         </div>
    </section>
    
     
    <!-- END section -->    
   <%@include file="js.jsp" %>
	</body>
</html>