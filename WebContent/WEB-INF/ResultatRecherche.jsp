 
<!DOCTYPE html>
<html lang="en">
	<head>
	
 <%@include file="css.jsp" %>
 
	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuClient.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover align-items-center  overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
	 <div class="overlay"></div>
	 <div class="container ">
	 	<div id="result-container">
	 	<div class="row col-md align-items-center table-responsive probootstrap-animate">
		 <div class="row col-md" id="try-other-link">  
		 	<a href="${link}" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Essayer une autre recherche ?</a>  
		 </div>     
	     <c:if test="${empty volsA}">
		  <div class="row no-result-found">
	    		<h2 id="" class="heading mb-2 display-4 font-light probootstrap-animate"><c:out value="${messageA}"/></h2>
	      </div>     
	     </c:if>
	     <c:if test="${!empty volsA}">
				<div class=" row col-md ">
				 	<h2 id="resultIndex1">Vols all�e disponibles pour vos sp�cifications</h2>	
					    <table class="table table-bordered" style="color:white;">
					      <thead>
					        <tr>
					          <th>N�</th>
					          <th>Id Vol</th>
					          <th>Ville D�part</th>
					          <th>Ville Arriv�e</th>
					          <th>Date D�part</th>
					          <th>Date Arriv�e</th>
					          <th>A�roport D�part</th>
					          <th>A�roport Arriv�e</th>
					          <th>Prix</th>
					          <th>Nom avion</th>
					          <th>Compagnie</th>
					          <th>Cat�gorie</th>
					          <th>Type avion</th>
					         <th>Places Disponibles</th>
					          <th>Poids max</th>
					        </tr>
					      </thead>
					      <tbody>
						      <c:forEach items="${volsA}" var="volA" varStatus="status">
								  <tr <c:if test="${volA.surplus == false && volA.poidsMax>65}">style="color:thistle;"</c:if>>
								  <td><c:out value="${status.count}"/></td><td><c:out value="${volA.id }"/></td>
								  <td><c:out value="${volA.villeD }"/></td>
								  <td><c:out value="${volA.villeA }"/></td>
								  <td><c:out value="${volA.dateD }"/></td>
								  <td><c:out value="${volA.dateA }"/></td>
								  <td><c:out value="${volA.aeroportD }"/></td>
								  <td><c:out value="${volA.aeroportA }"/></td>
								  <td><c:out value="${volA.prix }"/></td>
								  <td><c:out value="${volA.nom}"/></td>
								  <td><c:out value="${volA.compagnie}"/></td>
								  <td><c:out value="${volA.categorie}"/></td>
								  <td><c:out value="${volA.typeAvion}"/></td>
								  <td><c:out value="${volA.place}"/></td>
								  <td><c:out value="${volA.poidsMax}"/></td>
								  </tr>
							  </c:forEach>
						</tbody>
						</table>
				  </div>
	        </c:if>
	        
	        
	                 
          <c:if test="${empty volsEA}">
		  <div class="row no-result-found ">
	    		<h2 id="" class="heading mb-2 display-4 font-light probootstrap-animate"><c:out value="${messageEA}"/></h2>
	      </div>     
	     </c:if>
	     <c:if test="${!empty volsEA}">
				<div class=" row col-md ">
				 	<h2 id="resultIndex1">Vols escales all�e disponibles pour vos sp�cifications</h2>	
					    <table class="table table-bordered" style="color:white;">
					      <thead>
					        <tr>
					          <th>N�</th>
					          <th>Id Vol</th>
					          <th>Ville D�part</th>
					          <th>Ville Arriv�e</th>
					          <th>Date D�part</th>
					          <th>Date Arriv�e</th>
					          <th>A�roport D�part</th>
					          <th>A�roport Arriv�e</th>
					          <th>Prix d'ensemble</th>
					          <th>Temps Escale</th>
					          <th>Nom avion</th>
					          <th>Compagnie</th>
					          <th>Cat�gorie</th>
					          <th>Type avion</th>
					         <th>Places Disponibles</th>
					          <th>Poids max</th>
					        </tr>
					      </thead>
					      <tbody>
						      <c:forEach items="${volsEA}" var="volEA" varStatus="status">
								  <tr <c:if test="${volEA.surplus == false && volEA.poidsMax>65}">style="color:thistle;"</c:if>>
								  <td><c:out value="${status.count}"/></td><td><c:out value="${volEA.id }"/></td>
								  <td><c:out value="${volEA.villeD }"/></td>
								  <td><c:out value="${volEA.villeA }"/></td>
								  <td><c:out value="${volEA.dateD }"/></td>
								  <td><c:out value="${volEA.dateA }"/></td>
								  <td><c:out value="${volEA.aeroportD }"/></td>
								  <td><c:out value="${volEA.aeroportA }"/></td>
								  <td><c:out value="${volEA.prix }"/></td>
								  <td><c:out value="${volEA.tempsEscale }"/>H</td>
								  <td><c:out value="${volEA.nom}"/></td>
								  <td><c:out value="${volEA.compagnie}"/></td>
								  <td><c:out value="${volEA.categorie}"/></td>
								  <td><c:out value="${volEA.typeAvion}"/></td>
								  <td><c:out value="${volEA.place}"/></td>
								  <td><c:out value="${volEA.poidsMax}"/></td>
								  </tr>
							  </c:forEach>
						</tbody>
						</table>
				  </div>
	        </c:if>
          
          
          <c:if test="${empty volR}">
		   <div class=" row no-result-found">
	    		<h2 id="" class="heading mb-2 display-4 font-light probootstrap-animate"><c:out value="${messageR}"/></h2> 
	        </div>     
     	  </c:if>
     	  
     	   <c:if test="${!empty volsR}">
     	   	<div class=" row col-md">
			 		<h2 id="resultIndex2">Vols retour disponibles pour vos sp�cifications</h2>	
				    <table class="result-container table table-bordered" style="color:white;">
					      <thead>
					        <tr>
					          <th>N�</th>
					          <th>Id Vol</th>
					          <th>Ville D�part</th>
					          <th>Ville Arriv�e</th>
					          <th>Date D�part</th>
					          <th>Date Arriv�e</th>
					          <th>A�roport D�part</th>
					          <th>A�roport Arriv�e</th>
					          <th>Prix</th>
					          <th>Nom avion</th>
					          <th>Compagnie</th>
					          <th>Cat�gorie</th>
					          <th>Type avion</th>
					         <th>Places Disponibles</th>
					          <th>Poids max</th>
					        </tr>
					      </thead>
					      <tbody>
					      <c:forEach items="${volsR}" var="volR" varStatus="status">
						  <tr <c:if test="${volR.surplus == false && volR.poidsMax>65}">style="color:thistle;"</c:if>>
						  <td><c:out value="${status.count}"/></td>
						  <td><c:out value="${volR.id }"/></td>
						  <td><c:out value="${volR.villeD }"/></td>
						  <td><c:out value="${volR.villeA }"/></td>
						  <td><c:out value="${volR.dateD }"/></td>
						  <td><c:out value="${volR.dateA }"/></td>
						  <td><c:out value="${volR.aeroportD }"/></td>
						  <td><c:out value="${volR.aeroportA }"/></td>
						  <td><c:out value="${volR.prix }"/></td>
						  <td><c:out value="${volR.nom}"/></td>
						  <td><c:out value="${volR.compagnie}"/></td>
						  <td><c:out value="${volR.categorie}"/></td>
						  <td><c:out value="${volR.typeAvion}"/></td>
						  <td><c:out value="${volR.place}"/></td>
						  <td><c:out value="${volR.poidsMax}"/></td>
						  </tr>
						  </c:forEach>
						</tbody>
						</table>
				   </div>
          </c:if>
          
          
          
          <c:if test="${empty volER}">
		   <div class=" row no-result-found">
	    		<h2 id="" class="heading mb-2 display-4 font-light probootstrap-animate"><c:out value="${messageER}"/></h2> 
	        </div>     
     	  </c:if>
     	  
     	   <c:if test="${!empty volsER}">
     	   	<div class=" row col-md">
			 		<h2 id="resultIndex2">Vols Escales retour disponibles pour vos sp�cifications</h2>	
				    <table class="result-container table table-bordered" style="color:white;">
					      <thead>
					        <tr>
					          <th>N�</th>
					          <th>Id Vol</th>
					          <th>Ville D�part</th>
					          <th>Ville Arriv�e</th>
					          <th>Date D�part</th>
					          <th>Date Arriv�e</th>
					          <th>A�roport D�part</th>
					          <th>A�roport Arriv�e</th>
					          <th>Prix d'ensemble</th>
					          <th>Temps Escale</th>
					          <th>Nom avion</th>
					          <th>Compagnie</th>
					          <th>Cat�gorie</th>
					          <th>Type avion</th>
					         <th>Places Disponibles</th>
					          <th>Poids max</th>
					        </tr>
					      </thead>
					      <tbody>
					      <c:forEach items="${volsER}" var="volER" varStatus="status">
						  <tr <c:if test="${volER.surplus == false && volER.poidsMax>65}">style="color:thistle;"</c:if>>
						  <td><c:out value="${status.count}"/></td>
						  <td><c:out value="${volER.id }"/></td>
						  <td><c:out value="${volER.villeD }"/></td>
						  <td><c:out value="${volER.villeA }"/></td>
						  <td><c:out value="${volER.dateD }"/></td>
						  <td><c:out value="${volER.dateA }"/></td>
						  <td><c:out value="${volER.aeroportD }"/></td>
						  <td><c:out value="${volER.aeroportA }"/></td>
						  <td><c:out value="${volER.prix }"/></td>
						  <td><c:out value="${volER.tempsEscale }"/>H</td>
						  <td><c:out value="${volER.nom}"/></td>
						  <td><c:out value="${volER.compagnie}"/></td>
						  <td><c:out value="${volER.categorie}"/></td>
						  <td><c:out value="${volER.typeAvion}"/></td>
						  <td><c:out value="${volER.place}"/></td>
						  <td><c:out value="${volER.poidsMax}"/></td>
						  </tr>
						  </c:forEach>
						</tbody>
						</table>
				   </div>
          </c:if>
          
          
          </div>
       </div>
      </div>
    </section>
    
    <!-- END section -->    
    <%@include file="js.jsp" %>
	</body>
</html>