 
<!DOCTYPE html>
<html lang="en">
	<head>
	
    <%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuClient.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
   
      <div class="overlay"></div>
      <div class="container mainContainer">
        <div class="row align-items-center">
          <div class="col-md probootstrap-animate">
          <h2 style="color:white;margin-left:330px;">Rechercher un vol avec aeroport </h2>
            <form  class="probootstrap-form" method="post">
              <div class="form-group">
                <div class="row mb-3">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single">Aeroport de d�part</label>
                      <label for="id_label_single" style="width: 100%;">
                        <select name="aeroportD" class="js-example-basic-single js-states form-control" id="id_label_single" style="width: 100%;">
                           <c:forEach items="${aeroportsD}" var="aeroportD" varStatus="status">
		                      <option value="${aeroportD}">${aeroportD}</option>
							</c:forEach>
                        </select>
                      </label>

                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single2">Aeroport d'arriv�e</label>
                      <div class="probootstrap_select-wrap">
                        <label for="id_label_single2" style="width: 100%;">
                        <select name="aeroportA" class="js-example-basic-single js-states form-control" id="id_label_single2" style="width: 100%;">
                          <c:forEach items="${aeroportsA}" var="aeroportA" varStatus="status">
		                      <option value="${aeroportA}">${aeroportA}</option>
							</c:forEach>
                        </select>
                      </label>
                      </div>
                    </div>
                  </div>
                </div>
                 <!-- END row -->
                <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Date de d�part</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-calendar"></span> 
                        <input name="dateD" type="text" id="probootstrap-date-departure" class="form-control" placeholder="Choisir une date">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END row -->
                <div class="row">
                  <div class="col-md">
                    <label for="roundway"><input type="checkbox" value="true" id="roundway" name="volE">Vol Escale ?</label>
                  </div>
                  <div class="col-md">
                    <label for="oneway"><input type="checkbox" value="true" id="oneway" name="volR">Vol retour ?</label>
                  </div>
                  <div class="col-md">
                    <input type="submit" value="Chercher" class="btn btn-primary btn-block">
                  </div>
                </div>
            </form>
          </div> 
         </div>
          </div>
    </section>
    
     
    <!-- END section -->    
   <%@include file="js.jsp" %>
	</body>
</html>