 
<!DOCTYPE html>
<html lang="en">
	<head>
		
    <%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuAdmin.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container mainContainer">
        <div class="row align-items-center">
          <div class="col-md probootstrap-animate">
          <div class="row md-8 ajoutElement">
		     <h2>Ajout d'un nouveau vol</h2>
		   </div>
            <form  class="probootstrap-form" method="post">
              <div class="form-group">
                <div class="row mb-4">
                 <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Ville de d�part</label>
                        <input required="required" type="text" name="villeD"  class="form-control" placeholder="Ville de d�part">
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Ville d'arriv�e</label>
                        <input required="required" type="text" name="villeA"  class="form-control" placeholder="Ville d'arriv�e">
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Date de d�part</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-calendar"></span> 
                        <input name="dateD" type="text" id="probootstrap-date-departure" class="form-control" placeholder="Choisir une date">
                      </div>
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Date d'arriv�e</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-calendar"></span> 
                        <input name="dateA" type="text" id="probootstrap-date-departure" class="form-control" placeholder="Choisir une date">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                <!-- END row -->
              <div class="form-group">
                <div class="row mb-4">
                 <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Aeroport de d�part</label>
                        <input required="required" type="text" name="aeroportD"  class="form-control" placeholder="Aeroport de d�part">
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Aeroport d'arriv�e</label>
                        <input required="required" type="text" name="aeroportA"  class="form-control" placeholder="Aeroport d'arriv�e">
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Prix du vol</label>
                        <input required="required" type="text" name="prix"  class="form-control" placeholder="Prix du vol">
                    </div>
                  </div>
                   <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Temps escale</label>
                        <input required="required" type="text" name="tempsEscale"  class="form-control" placeholder="En heures ( ex:6)">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row mb-2">
                  <div class="col-md">
                    <div class="form-group">
                        <input required="required" type="text" name="idAvion"  class="form-control" placeholder="Id de l'avion qui va effectuer le vol">
                    </div>
                  </div>
                  <div class="col-md">
                    <input type="submit" value="Ajouter" class="btn btn-primary btn-block">
                  </div>
                </div>
              </div>
                <!-- END row -->
            </form>
          </div>
          </div>  
         </div>
    </section>
    
     
    <!-- END section -->    
   <%@include file="js.jsp" %>
	</body>
</html>