 
<!DOCTYPE html>
<html lang="en">
	<head>
		
<%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuAdmin.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container mainContainer">
        <div class="row align-items-center">
          <div class="col-md probootstrap-animate">
          <div class="row md-8 ajoutElement">
		     <h2>Recherche d'un avion</h2>
		   </div>
            <form  class="probootstrap-form" method="post">
              <div class="form-group">
                <div class="row mb-2">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Catégorie des avions</label>
                        <label for="id_label_single1" style="width: 100%;">
                        <select name="categorie" class="js-example-basic-single js-states form-control" id="id_label_single1" style="width: 100%;">
                          <option selected value="tous">tous</option>
                          <c:forEach items="${categories}" var="categorie" varStatus="status">
		                      <option value="${categorie}">${categorie}</option>
							</c:forEach>
                        </select>
                      </label>
                    </div>
                  </div>
                <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single">Type de l'avion</label>
                      <label for="id_label_single" style="width: 100%;">
                       <input  type="hidden" id="RechercheOperation" name="operation"  class="form-control">
                        <select name="typeAvion" class="js-example-basic-single js-states form-control" id="id_label_single" style="width: 100%;">
                           <option selected value="tous">tous</option>
                           <c:forEach items="${typesAvion}" var="typeAvion" varStatus="status">
		                      <option value="${typeAvion}">${typeAvion}</option>
							</c:forEach>
                        </select>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row mb-2">
                  <div class="col-md">
                    <input type="submit" id="Rechercher" value="Rechercher" class="btn btn-primary btn-block">
                  </div>
                </div>
               </div>
            </form>
          </div>
          </div>  
         </div>
    </section>
    
     
    <!-- END section -->    
   <%@include file="js.jsp" %>
	</body>
</html>