
    <nav class="navbar navbar-expand-lg navbar-dark  probootstrap_navbar" id="probootstrap-navbar">
      <div class="container">
        <a class="navbar-brand" href="Index">Benin Airline</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-menu" aria-controls="probootstrap-menu" aria-expanded="false" aria-label="Toggle navigation">
          <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-menu">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a class="nav-link" href="Index">Accueil</a></li>
            <c:if test="${!empty sessionScope.mail}">
            <li class="nav-item"><a class="nav-link" href="Logout">Se d�connecter</a></li> 
			</c:if>
			<c:if test="${empty sessionScope.mail}">
				<li class="nav-item"><a class="nav-link" href="Register">Cr�er compte</a></li>
            <li class="nav-item"><a class="nav-link" href="Login">Se connecter</a></li>
			</c:if>
            <li class="nav-item"><a class="nav-link" href="#">
			<c:if test="${!empty sessionScope.mail}">
				<p style="color:blue;"><c:out value="Connect�"/></p>
			</c:if>
			<c:if test="${empty sessionScope.mail}">
			<p style="color:blue;"><c:out value="D�connect�"/></p>
			</c:if>
            </a></li>
            <!-- <li class="nav-item "><a class="nav-link" href="Dashboard.jsp">Admin Dashboard</a></li>-->
            <li class="nav-item dropdown">
		      <a class="nav-link dropdown-toggle fa fa-user fa-3x" href="#" id="navbardrop" data-toggle="dropdown">
		        Recherche 
		        personnalis�e
		      </a>
		      <div class="dropdown-menu">
		        <a class="dropdown-item" href="ListVolsAvecPoids">Recherche par poids</a>
		        <a class="dropdown-item" href="ListVolSelonTypeBagage">Recherche par type de bagage</a>
		        <a class="dropdown-item" href="ListVolsAvecAeroport">Recherche par aeroport</a>
		      </div>
    		</li>
          </ul>
        </div>
      </div>
    </nav>
    
   