 
<!DOCTYPE html>
<html lang="en">
	<head>
	
 <%@include file="css.jsp" %>
 
	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuAdmin.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover align-items-center  overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
	 <div class="overlay"></div>
	 <div class="container ">
		   <c:if test="${!empty avions}">
		      	<div class="container">
		      	<c:if test="${!empty message}">
			      	<div class="row col-md" id="try-other-recherche-link">  
					 	<a href="RechercheAvion" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3"><c:out value="${message}"/></a>  
					</div> 
				</c:if>
					<div class="row col-md align-items-center table-responsive probootstrap-animate">
					 <h2 style="color:white; margin-left:340px;">Liste des avions</h2>		
						 <table style="color:white;" class="table table-bordered">
						      <thead>
						        <tr>
						          <th>N�</th>
						          <th>ID</th>
						          <th>Nom</th>
						          <th>Compangie</th>
						          <th>Categorie</th>
						          <th>Type Avion</th>
						          <th>Nbre de place</th>
						          <th>Poids Max</th>
						          <th>Surplus</th>
						           <th>Actions</th>
						        </tr>
						      </thead>
						      <tbody>
						      <c:forEach items="${avions}" var="avion" varStatus="status">
							  <tr><td><c:out value="${status.count}"/></td>
							  <td><c:out value="${avion.id }"/></td>
							  <td><c:out value="${avion.nom }"/></td>
							  <td><c:out value="${avion.compagnie }"/></td>
							  <td><c:out value="${avion.categorie }"/></td>
							  <td><c:out value="${avion.typeAvion }"/></td>
							  <td><c:out value="${avion.place }"/></td>
							  <td><c:out value="${avion.poidsMax }"/></td>
							  <td><c:if test="${avion.surplus==true}">oui</c:if><c:if test="${avion.surplus==false}">non</c:if></td>
							  <td>
							  	<c:if test="${avion.categorie == 'AirLourd'}">
								  	<button name="${avion.id },${avion.nom },${avion.poidsMax },${categorie},${typeAvion}" type="button" class="btn btn-primary configurerAvionsRecherches" data-toggle="modal" data-target="#myModalConfigurerAvionsRecherches">
	  								Modifier PoidsMax
									</button>
								</c:if>
							  </td>
							  </tr>
							  </c:forEach>
						      </tbody>
				    	</table>
					  </div>
					</div>
	          </c:if>
          
          </div>
    </section>
    
    	<!-- The Modal -->
								<div class="modal fade" id="myModalConfigurerAvionsRecherches">
								  <div class="modal-dialog">
								    <div class="modal-content">
								
								      <!-- Modal Header -->
								      <div class="modal-header">
								        <h4 class="modal-title">Modication du poids maximal de l'avion <c:out value="${avion.nom }"/></h4>
								      </div>
								
								      <!-- Modal body -->
								      <div class="modal-body">
								          <form  class="form" method="post">
								              <div class="form-group">
								                 <div class="row mb-4">
								                  <div class="col-md">
								                    <div class="form-group">
								                      <label>Id de l'avion</label>
								                       <label id="label-id" style="width: 100%;">
								                       </label>
								                     <input id ="categorie" type="hidden" name="categorie"  class="form-control">
								                     <input id ="typeAvion" type="hidden" name="typeAvion"  class="form-control">
								                     <input  type="hidden" id="ConfigurationOperation" name="operation"  class="form-control">
								                     <input id ="idAvion" type="hidden" name="idAvion"  class="form-control">
								                    </div>
								                  </div>
								                  <div class="col-md">
								                    <div class="form-group">
								                      <label >Nom de l'avion </label>
								                    	<label id="label-nom" style="width: 100%;">
								                      		
								                      	</label>
								                    </div>
								                  </div>
								                </div>
								              
								                <div class="row mb-4">
								                  <div class="col-md">
								                    <div class="form-group">
								                      <label>Actuel poids maximal</label>
								                      <label id="label-poidsMax" style="width: 100%;">
								                             
								                      </label>
								                    </div>
								                  </div>
								                  <div class="col-md">
								                    <div class="form-group">
								                      <label >Nouveau poids maximal</label>
								                        <input required="required" id="poidsMax" type="text" name="poidsMax"  class="form-control" placeholder="Poids maximal">
								                    </div>
								                  </div>
								                </div>
									            <div class="row mb-2">
													<div class="col-md">
													   <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
													</div>
										            <div class="col-md">
										                 <input type="submit" id="Modifier" value="Modifier" class="btn btn-primary btn-block">
										            </div>
									             </div>
								              </div>
								            </form>
								      </div>
								    </div>
								  </div>
							</div>
    
    <!-- END section -->    
    <%@include file="js.jsp" %>
	</body>
</html>