 
<!DOCTYPE html>
<html lang="en">
	<head>
		  
    <%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuClient.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
   
      <div class="overlay"></div>
      <div class="container mainContainer">
        <div class="row align-items-center">
          <div class="col-md probootstrap-animate">
          <h2 style="color:white;margin-left:330px;">Rechercher un vol avec date </h2>
            <form  class="probootstrap-form" method="post">
              <div class="form-group">
                <div class="row mb-3">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single">Ville de d�part</label>
                      <label for="id_label_single" style="width: 100%;">
                        <select name="villeD" class="js-example-basic-single js-states form-control" id="id_label_single" style="width: 100%;">
                           <c:forEach items="${villesD}" var="villeD" varStatus="status">
		                      <option value="${villeD}">${villeD}</option>
							</c:forEach>
                        </select>
                      </label>

                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single2">Ville d'arriv�e</label>
                      <div class="probootstrap_select-wrap">
                        <label for="id_label_single2" style="width: 100%;">
                        <select name="villeA" class="js-example-basic-single js-states form-control" id="id_label_single2" style="width: 100%;">
                          <c:forEach items="${villesA}" var="villeA" varStatus="status">
		                      <option value="${villeA}">${villeA}</option>
							</c:forEach>
                        </select>
                      </label>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END row -->
                <div class="row mb-5">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Date de d�part</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-calendar"></span> 
                        <input name="dateD" type="text" id="probootstrap-date-departure" class="form-control" placeholder="Choisir une date">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END row -->
                <div class="row">
                 <div class="col-md">
                    <label for="oneway"><input type="checkbox" value="true" id="oneway" name="volR">Inclure Vol retour</label>
                  </div>
                  <div class="col-md">
                    <input type="submit" value="Chercher" class="btn btn-primary btn-block">
                  </div>
                </div>
              </div>
            </form>
          </div> 
         </div>
          </div>
    </section>
    
     
    <!-- END section -->    
  <%@include file="js.jsp" %>
	</body>
</html>