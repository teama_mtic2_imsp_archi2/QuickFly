 
<!DOCTYPE html>
<html lang="en">
	<head>
	 
    <%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuAdmin.jsp" %>	
    <!-- END nav -->
    </div>
   <section class="probootstrap-cover align-items-center  overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
     <c:if test="${empty avions}">
	   <div class="container">
    		<h2 id="no-result-found" class="heading mb-2 display-4 font-light probootstrap-animate"><c:out value="Vous n'avez pas encore ajout� de vols"/></h2>
             <div class="row col-md" id="no-result-link">  <a href="AjouterAvion" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Ajouter un vol ?</a>  
        </div>  
       </div>     
     </c:if>
       <c:if test="${!empty avions}">
           <div class="overlay"></div>
      	<div class="container mainContainer">
			<div class="row col-md align-items-center table-responsive probootstrap-animate">
			 <h2 style="color:white; margin-left:400px;">Liste des Avions </h2>	
				    <table class="table table-bordered" style="color:white;">
				     <thead>
			        <tr>
			          <th>N�</th>
			          <th>ID</th>
			          <th>Nom</th>
			          <th>Compangie</th>
			          <th>Categorie</th>
			          <th>Type Avion</th>
			          <th>Place</th>
			          <th>Poids Max</th>
			          <th>Surplus</th>
			        </tr>
			      </thead>
			      <tbody>
			      <c:forEach items="${avions}" var="avion" varStatus="status">
				  <tr><td><c:out value="${status.count}"/></td><td><c:out value="${avion.id }"/></td><td><c:out value="${avion.nom }"/></td>
				  <td><c:out value="${avion.compagnie }"/></td><td><c:out value="${avion.categorie }"/></td><td><c:out value="${avion.typeAvion }"/></td>
				  <td><c:out value="${avion.place }"/></td><td><c:out value="${avion.poidsMax }"/></td>
				  <td><c:if test="${avion.surplus==true}">oui</c:if><c:if test="${avion.surplus==false}">non</c:if></td>
				  </tr>
				  </c:forEach>
			      </tbody>
				    </table>
			  </div>
			  </div>
          </c:if>
    </section>
   
    <!-- END section -->  
      
   <%@include file="js.jsp" %>
	</body>
</html>