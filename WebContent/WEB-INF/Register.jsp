
<!DOCTYPE html>
<html lang="en">
	<head>
	 <%@include file="css.jsp" %>
	</head>
	<body>
  <%@include file="MenuClient.jsp" %>
    <!-- END nav -->
    <!-- END section -->
    
    
    <section class="probootstrap_section bg-light" id="section-contact">
      <div class="container">
        
        <div class="row">
          <div class="col-md-6 probootstrap-animate">
            <p class="mb-5">Benin Airline is always entirely at your Disposal for moving every where in the world.
            We have about 10 years of experience in Traveling,We move over 50 countries in world.
            Feel free to contact us for any enquiry about traveling</p>
            <div class="row">
              <div class="col-md-6">
                <ul class="probootstrap-contact-details">
                  <li>
                    <span class="text-uppercase"><span class="ion-paper-airplane"></span> Email</span>
                    sidoine.ode@imsp-uac.org
                  </li>
                  <li>
                    <span class="text-uppercase"><span class="ion-ios-telephone"></span> Phone</span>
                    +229 69 26 88 09
                  </li>
                </ul>
              </div>
              <div class="col-md-6">
                <ul class="probootstrap-contact-details">
                  <li>
                    <span class="text-uppercase"><span class="ion-ios-telephone"></span> Fax</span>
                    +229 66 09 19 13
                  </li>
                  <li>
                    <span class="text-uppercase"><span class="ion-location"></span> Profile Company</span>
                    Sidoine ODE <br>
                    BIMENYIMANA Ildegard Christian  <br>
                    DEGBO Olivier Berly <br>
                    NGENZIRABONA Emmanuel 
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-6  probootstrap-animate">
          <h2 style="color:green;margin-left:50px;">Page de cr�ation de compte</h2>
            <form method="post" class="probootstrap-form probootstrap-form-box mb60">
            <c:if test="${!empty error}"><p style="color:red; text-align:center;"><c:out value="${error}"/></p></c:if>
		  	<c:if test="${!empty success}"><p style="color:green; text-align:center;"><c:out value="${success}"/></p></c:if>
              <div class="form-group">
              </div>
              <div class="form-group">
                <label for="email" class="required sr-only sr-only-focusable">Login (E-mail)</label>
                <input type="email" class="form-control" id="email" name="mail" placeholder="Login (E-mail)">
              </div>
              <div class="form-group">
                  <label for="email" class="required sr-only sr-only-focusable">Password</label>
                  <input type="password" required ="required" class="form-control" id="email" name="mdp" placeholder="taper mot de passe">
              </div>
              <div class="form-group">
                  <label for="email" class="required sr-only sr-only-focusable">Password</label>
                  <input type="password" required ="required" class="form-control" id="email" name="mdpbis" placeholder="Retaper mot de passe">
              </div>
              <div class="form-group">
                <input type="submit" required ="required" class="btn btn-primary" id="submit" name="submit" value="Cr�er compte">
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->
 
 <%@include file="js.jsp" %>

	</body>
</html>