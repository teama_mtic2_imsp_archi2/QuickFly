
    <nav class="navbar navbar-expand-lg navbar-dark  probootstrap_navbar" id="probootstrap-navbar">
      <div class="container">
        <a class="navbar-brand" href="Index">Benin Airline</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-menu" aria-controls="probootstrap-menu" aria-expanded="false" aria-label="Toggle navigation">
          <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-menu">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a class="nav-link" href="IndexAdmin">Accueil</a></li>
            <c:if test="${!empty sessionScope.mail}">
            	<li class="nav-item"><a class="nav-link" href="Logout">Se déconnecter</a></li>
			</c:if>
            <li class="nav-item"><a class="nav-link" href="#">
			<c:if test="${!empty sessionScope.mail}">
				<p style="color:blue;"><c:out value="Connecté"/></p>
			</c:if>
			<c:if test="${empty sessionScope.mail}">
			<p style="color:blue;"><c:out value="Déconnecté"/></p>
			</c:if>
            </a></li>
            <!-- <li class="nav-item "><a class="nav-link" href="Dashboard.jsp">Admin Dashboard</a></li>-->
            <li class="nav-item dropdown">
		      <a class="nav-link dropdown-toggle fa fa-user fa-3x" href="#" id="navbardrop" data-toggle="dropdown">
		    	Menu Admin
		      </a>
		      <div class="dropdown-menu">
		        <a class="dropdown-item" href="AjouterAvion">Ajouter un avion</a>
		        <a class="dropdown-item" href="AjouterVol">Ajouter un vol</a>
		        <a class="dropdown-item" href="ListAvion">Lister les avions</a>
		        <a class="dropdown-item" href="ListVol">Lister les vols</a>
		        <a class="dropdown-item" href="RechercheAvion">Rechercher et Configurer</a>
		        <a class="dropdown-item" href="ConfigurerAvionParCategorie">Configurer les avions AirLourd</a>
		      </div>
    		</li>
          </ul>
        </div>
      </div>
    </nav>
    
   