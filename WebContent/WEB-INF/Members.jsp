 
<!DOCTYPE html>
<html lang="en">
	<head>
		 <%@include file="css.jsp" %>

	</head>
	<body>
	<div class="row">
  	<!-- START nav -->
  	<%@include file="MenuClient.jsp" %>	
    <!-- END nav -->
    </div>
    
   <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container mainContainer">
        <div class="row align-items-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Explore The World With Benin Airline</h2>
            <p class="lead mb-5 probootstrap-animate">
           	<a href="#" target="_blank"> QuickyFly members</a> 
           	Project <a href="Members" target="_blank">MTIC 2 2018-2019</a>
           	</p>
           	<div class="row p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Projecct members</div>
	        <div class="row"><a href="#"  class="p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Sidoine ODE</a> </div> 
	        <div class="row"><a href="#"  class="p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">BIMENYIMANA Ildegard Christian</a> </div> 
	        <div class="row"><a href="#"  class="p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">B�ryl DEGBO</a> </div> 
	        <div class="row"><a href="#"  class="p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">NGENZIRABONA Emmanuel</a> </div> 
       
          </div>
         </div>
          </div>
    </section>
    
     
    <!-- END section -->    
   <%@include file="js.jsp" %>
	</body>
</html>